#include "Checkpoints.h"
#include "ConsoleMessages.h"
#include "Utils.h"
extern PluginFuncs* gFuncs;

int SphereCheckpoint::CreateCheckpoint(Player player, int32_t world, uint8_t isSphere, Vector3 position, RGBA colour, float radius, float angle)
{
	if (this->b_Initialized)
	{
		OutputError("Error in SphereCheckpoint::CreateCheckpoint. Cannot intialize the same checkpoint twice.");
		return -1;
	}

	if (!player.IsValid())
	{
		OutputError("Error in SphereCheckpoint::CreateCheckpoint. Player instance is invalid.");
		return -1;
	}

	//int checkpoint_id = gFuncs->CreateCheckPoint(player.GetPlayerID(), world, isSphere, position.x, position.y, position.z, colour.r, colour.g, colour.b, colour.a, radius);
	//if (checkpoint_id < 0)
	//{
	//	OutputError("Error in SphereCheckpoint::CreateCheckpoint. %s.", GetErrorMessage(gFuncs->GetLastError()));
	//	return -1;
	//}

	Vector3 relativePosition;
	int pickup_id = -1;
	for (int i = 0; i < 6; ++i)
	{
		switch (i)
		{
		case 0:
			pickup_id = gFuncs->CreatePickup(382, 1, 0, position.x, position.y, position.z, 255, 0);
			if (pickup_id < 0)
			{
				OutputError("Error in SphereCheckpoint::CreateCheckpoint. %s.", GetErrorMessage(gFuncs->GetLastError()).c_str());
				return -1;
			}
			else
				this->pickupCheckpoints.at(0) = pickup_id;
			break;

		case 1:
			pickup_id = gFuncs->CreatePickup(382, 1, 0, position.x, position.y, position.z + 3.0f, 255, 0);
			if (pickup_id < 0)
			{
				OutputError("Error in SphereCheckpoint::CreateCheckpoint. %s.", GetErrorMessage(gFuncs->GetLastError()).c_str());
				return -1;
			}
			else
				this->pickupCheckpoints.at(1) = pickup_id;
			break;

		case 2:
			relativePosition = Utils::GetRightPosition(position, angle, 3.0f);
			pickup_id = gFuncs->CreatePickup(382, 1, 0, relativePosition.x, relativePosition.y, relativePosition.z, 255, 0);
			if (pickup_id < 0)
			{
				OutputError("Error in SphereCheckpoint::CreateCheckpoint. %s.", GetErrorMessage(gFuncs->GetLastError()).c_str());
				return -1;
			}
			else
				this->pickupCheckpoints.at(2) = pickup_id;
			break;

		case 3:
			relativePosition = Utils::GetRightPosition(position, angle, 3.0f);
			pickup_id = gFuncs->CreatePickup(382, 1, 0, relativePosition.x, relativePosition.y, relativePosition.z + 3.0f, 255, 0);
			if (pickup_id < 0)
			{
				OutputError("Error in SphereCheckpoint::CreateCheckpoint. %s.", GetErrorMessage(gFuncs->GetLastError()).c_str());
				return -1;
			}
			else
				this->pickupCheckpoints.at(3) = pickup_id;
			break;

		case 4:
			relativePosition = Utils::GetLeftPosition(position, angle, 3.0f);
			pickup_id = gFuncs->CreatePickup(382, 1, 0, relativePosition.x, relativePosition.y, relativePosition.z, 255, 0);
			if (pickup_id < 0)
			{
				OutputError("Error in SphereCheckpoint::CreateCheckpoint. %s.", GetErrorMessage(gFuncs->GetLastError()).c_str());
				return -1;
			}
			else
				this->pickupCheckpoints.at(4) = pickup_id;
			break;

		case 5:
			relativePosition = Utils::GetLeftPosition(position, angle, 3.0f);
			pickup_id = gFuncs->CreatePickup(382, 1, 0, relativePosition.x, relativePosition.y, relativePosition.z + 3.0f, 255, 0);
			if (pickup_id < 0)
			{
				OutputError("Error in SphereCheckpoint::CreateCheckpoint. %s.", GetErrorMessage(gFuncs->GetLastError()).c_str());
				return -1;
			}
			else
				this->pickupCheckpoints.at(5) = pickup_id;
			break;
		}
	}


	this->b_Initialized = true;
	this->nCheckpointId = this->pickupCheckpoints.at(0);
	this->heading = angle;
	OutputInfo("Creating checkpoint at [%s] for player [%d] with radius [%.3f] with ID [%d].", static_cast<std::string>(position).c_str(), player.GetPlayerID(), radius, this->pickupCheckpoints.at(0));
	return this->pickupCheckpoints.at(0);
}

unsigned int SphereCheckpoint::GetCheckpointWorld()
{
	if (!this->b_Initialized)
	{
		OutputError("Error in SphereCheckpoint::GetCheckpointWorld. Unitialized checkpoint.");
		return -1;
	}

	else return gFuncs->GetCheckPointWorld(this->nCheckpointId);
}

void SphereCheckpoint::SetCheckpointWorld(int32_t world)
{
	if (!this->b_Initialized)
	{
		OutputError("Error in SphereCheckpoint::SetCheckpointWorld. Unitialized checkpoint.");
		return;
	}

	gFuncs->SetCheckPointWorld(this->nCheckpointId, world);
}

vcmpError SphereCheckpoint::SetCheckpointPosition(Vector3 position, float newAngle)
{
	if (!this->b_Initialized)
	{
		OutputError("Error in SphereCheckpoint::SetCheckpointPosition. Unitialized checkpoint.");
		return vcmpErrorNoSuchEntity;
	}

	vcmpError error;
	Vector3 relativePosition;
	for (int i = 0; i < 6; ++i)
	{
		switch(i)
		{
		case 0:
			error = gFuncs->SetPickupPosition(this->pickupCheckpoints.at(i), position.x, position.y, position.z);
			if (error)
			{
				OutputError("Error in SphereCheckpoint::SetCheckpointPosition. %s.", GetErrorMessage(gFuncs->GetLastError()).c_str());
				return error;
			}
			break;

		case 1:
			error = gFuncs->SetPickupPosition(this->pickupCheckpoints.at(i), position.x, position.y, position.z + 3.0f);
			if (error)
			{
				OutputError("Error in SphereCheckpoint::SetCheckpointPosition. %s.", GetErrorMessage(gFuncs->GetLastError()).c_str());
				return error;
			}
			break;

		case 2:
			relativePosition = Utils::GetRightPosition(position, newAngle, 3.0f);
			error = gFuncs->SetPickupPosition(this->pickupCheckpoints.at(i), relativePosition.x, relativePosition.y, relativePosition.z);
			if (error)
			{
				OutputError("Error in SphereCheckpoint::SetCheckpointPosition. %s.", GetErrorMessage(gFuncs->GetLastError()).c_str());
				return error;
			}

			break;

		case 3:
			relativePosition = Utils::GetRightPosition(position, newAngle, 3.0f);
			error = gFuncs->SetPickupPosition(this->pickupCheckpoints.at(i), relativePosition.x, relativePosition.y, relativePosition.z + 3.0f);
			if (error)
			{
				OutputError("Error in SphereCheckpoint::SetCheckpointPosition. %s.", GetErrorMessage(gFuncs->GetLastError()).c_str());
				return error;
			}

			break;

		case 4:
			relativePosition = Utils::GetLeftPosition(position, newAngle, 3.0f);
			error = gFuncs->SetPickupPosition(this->pickupCheckpoints.at(i), relativePosition.x, relativePosition.y, relativePosition.z);
			if (error)
			{
				OutputError("Error in SphereCheckpoint::SetCheckpointPosition. %s.", GetErrorMessage(gFuncs->GetLastError()).c_str());
				return error;
			}
			break;

		case 5:
			relativePosition = Utils::GetLeftPosition(position, newAngle, 3.0f);
			error = gFuncs->SetPickupPosition(this->pickupCheckpoints.at(i), relativePosition.x, relativePosition.y, relativePosition.z + 3.0f);
			if (error)
			{
				OutputError("Error in SphereCheckpoint::CreateCheckpoint. %s.", GetErrorMessage(gFuncs->GetLastError()).c_str());
				return error;
			}
			break;
	}
		if (error)
			OutputMessage("Error in SphereCheckpoint::SetCheckpointPosition. %s", GetErrorMessage(error).c_str());
	}

	return error;
}

void SphereCheckpoint::Respawn(Player player, int32_t world, Vector3 position, RGBA colour, float radius)
{
	if(!this->b_Initialized)
	{
		OutputError("Error in SphereCheckpoint::SetCheckpointPosition. Unitialized checkpoint.");
		return;
	}


	this->Delete();
	this->CreateCheckpoint(player, world, 0, position, colour, radius, this->heading);
}

Vector3 SphereCheckpoint::GetCheckpointPosition()
{
	if (!this->b_Initialized)
	{
		OutputError("Error in SphereCheckpoint::GetCheckpointPosition. Unitialized checkpoint.");
		return Vector3();
	}

	float fX, fY, fZ;
	vcmpError error = gFuncs->GetPickupPosition(this->pickupCheckpoints.at(0), &fX, &fY, &fZ);
	if (error)
	{
		OutputError("Error in SphereCheckpoint::GetCheckpointPosition. %s.", GetErrorMessage(error).c_str());
		return Vector3();
	}

	return Vector3(fX, fY, fZ);
}

void SphereCheckpoint::Delete()
{
	if (!this->b_Initialized)
	{
		OutputError("Error in SphereCheckpoint::Delete. Unitialized checkpoint.");
		return;
	}

	for (int i = 0; i < 6; ++i)
		gFuncs->DeletePickup(this->pickupCheckpoints.at(i));
	this->b_Initialized = false;
}

bool SphereCheckpoint::IsValid()
{
	return this->b_Initialized;
}
