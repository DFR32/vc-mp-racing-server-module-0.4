#pragma once
#include "plugin.h"
#include "Player.h"
#include "Vector3.h"
#include <vector>

struct RGBA
{
public:
	int r, g, b, a;

	RGBA()
	{
		r = 0;
		g = 0;
		b = 0;
		a = 0;
	}

	RGBA(int _r, int _g, int _b, int _a) :
		r(_r),
		g(_g),
		b(_b),
		a(_a)
	{};

	~RGBA() {}
};

class SphereCheckpoint
{
private:
	int nCheckpointId;
	bool b_Initialized;
	std::vector<unsigned int> pickupCheckpoints;
	float heading;

public:
	SphereCheckpoint()
		:
		nCheckpointId(-1),
		b_Initialized(false),
		pickupCheckpoints(6, 0),
		heading(0.0f)
	{}

	~SphereCheckpoint() {}

	int CreateCheckpoint(Player player, int32_t world, uint8_t isSphere, Vector3 position, RGBA colour, float radius, float angle);

	unsigned int GetCheckpointWorld();
	void SetCheckpointWorld(int32_t world);

	vcmpError SetCheckpointPosition(Vector3 position, float newAngle);
	void Respawn(Player player, int32_t world, Vector3 position, RGBA colour, float radius);
	Vector3 GetCheckpointPosition();

	void Delete();

	bool IsValid();
};
