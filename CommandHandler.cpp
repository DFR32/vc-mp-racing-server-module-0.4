#include "CommandHandler.h"
#include "ConsoleMessages.h"
#include "Utils.h"

void CommandHandler::Register(std::string command, void* FunctionToCall)
{
	this->Commands.push_back(std::pair<std::string, void*>(Utils::LowerStr(command), FunctionToCall));
}

void * CommandHandler::Get(std::string command)
{
	std::string loweredComand = Utils::LowerStr(command);
	for (auto & cmd : this->Commands)
	{
		if (static_cast<std::string>(cmd.first) == loweredComand)
			return static_cast<void*>(cmd.second);
	}
	return nullptr;
}

bool CommandHandler::Invoke(int nPlayerId, std::string command)
{
	void* commandFunction = this->Get(command);
	if (commandFunction)
	{
		stdCommand_t stdCommand = reinterpret_cast<stdCommand_t>(commandFunction);
		stdCommand(nPlayerId, std::string(""));

		OutputVerboseInfo("Player [%d] invoked command [%s] without arguments.", nPlayerId, command.c_str());
			return true;
	}

	OutputWarning("Could not invoke the command [%s]. Warning in CommandHandler::Invoke", command.c_str());
	return false;
}

bool CommandHandler::Invoke(int nPlayerId, std::string command, std::string arguments)
{
	void* commandFunction = this->Get(command);
	if (commandFunction)
	{
		stdCommand_t stdCommand = reinterpret_cast<stdCommand_t>(commandFunction);
		stdCommand(nPlayerId, arguments);

		OutputVerboseInfo("Player [%d] invoked command [%s] with arguments [%s].", nPlayerId, command.c_str(), arguments.c_str());
		return true;
	}

	OutputWarning("Could not invoke the command [%s] with arguments [%s]. Warning in CommandHandler::Invoke", command.c_str(), arguments.c_str());
	return false;
}

size_t CommandHandler::GetCommandBlockSize()
{
	return this->Commands.size();
}
