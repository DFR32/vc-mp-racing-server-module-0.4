#pragma once
#include <vector>
#include <string>

typedef void(* stdCommand_t)(int nPlayerId, std::string arguments);

class CommandHandler
{
	friend class PlayerPool;
private:
	std::vector<std::pair<std::string, void*> > Commands;

public:
	void Register(std::string command, void* FunctionToCall);
	void * Get(std::string command);
	bool Invoke(int nPlayerId, std::string command);
	bool Invoke(int nPlayerId, std::string command, std::string arguments);
	size_t GetCommandBlockSize();
};
