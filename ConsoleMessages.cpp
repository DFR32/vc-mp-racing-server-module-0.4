#include "ConsoleMessages.h"
#include <cstdio>
#include <cstdarg>
#include <fstream>

#if defined(WIN32) || defined(_WIN32)
#include <Windows.h>
#endif
#include <iostream>

std::ofstream logFile;
std::ofstream bugreportFile;

void WriteToLogFile(std::string str)
{
	if (!logFile.is_open())
	{
		logFile.open("logfile.txt", std::ofstream::out | std::ofstream::app);
		if (!logFile.is_open())
		{
			OutputError("Error in WriteToLogFile. logFile isn't open and couldn't be opened.");
			return;
		}
	}

	logFile << str << std::endl;
}

void WriteToBugReportFile(std::string str)
{
	if (!bugreportFile.is_open())
	{
		bugreportFile.open("bug_reports.txt", std::ofstream::out | std::ofstream::app);
		if (!bugreportFile.is_open())
		{
			OutputError("Error in WriteToBugReportFile. logFile isn't open and couldn't be opened.");
			return;
		}
	}

	bugreportFile << str << std::endl;
}


void OutputError(const char* error, ...)
{
	char buf[512];
	va_list begin;

	va_start(begin, error);
	vsnprintf(buf, 512, error, begin);
	va_end(begin);

	#if defined(WIN32) || defined(_WIN32)
	HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_SCREEN_BUFFER_INFO csbBefore;
	GetConsoleScreenBufferInfo(hstdout, &csbBefore);
	SetConsoleTextAttribute(hstdout, FOREGROUND_RED);
	printf("[ERROR]: ");

	SetConsoleTextAttribute(hstdout, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);
	printf("%s\n", buf);

	SetConsoleTextAttribute(hstdout, csbBefore.wAttributes);
	#else
	printf("[ERROR]: %s\n", error);
	#endif

	WriteToLogFile("[ERROR:] " + std::string(buf));
}

void OutputMessage(const char* msg, ...)
{
	char buf[512];
	va_list begin;

	va_start(begin, msg);
	vsnprintf(buf, 512, msg, begin);
	va_end(begin);

#if defined(WIN32) || defined(_WIN32)
	HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_SCREEN_BUFFER_INFO csbBefore;
	GetConsoleScreenBufferInfo(hstdout, &csbBefore);
	SetConsoleTextAttribute(hstdout, FOREGROUND_GREEN);
	printf("[MESSAGE]: ");

	SetConsoleTextAttribute(hstdout, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);
	printf("%s\n", buf);

	SetConsoleTextAttribute(hstdout, csbBefore.wAttributes);
#else
	printf("[MODULE]: %s\n", buf);
#endif
	WriteToLogFile("[MESSAGE:] " + std::string(buf));
}

void OutputInfo(const char* msg, ...)
{
	char buf[512];
	va_list begin;

	va_start(begin, msg);
	vsnprintf(buf, 512, msg, begin);
	va_end(begin);

#if defined(WIN32) || defined(_WIN32)
	HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_SCREEN_BUFFER_INFO csbBefore;
	GetConsoleScreenBufferInfo(hstdout, &csbBefore);
	SetConsoleTextAttribute(hstdout, FOREGROUND_RED | FOREGROUND_BLUE);
	printf("[INFO]: ");

	SetConsoleTextAttribute(hstdout, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);
	printf("%s\n", buf);

	SetConsoleTextAttribute(hstdout, csbBefore.wAttributes);
#else
	printf("[INFO]: %s\n", buf);
#endif

	WriteToLogFile("[INFO:] " + std::string(buf));
}

#define OUTPUT_VERBOSE
void OutputVerboseInfo(const char* msg, ...)
{
#ifdef OUTPUT_VERBOSE
	char buf[512];
	va_list begin;

	va_start(begin, msg);
	vsnprintf(buf, 512, msg, begin);
	va_end(begin);

#if defined(WIN32) || defined(_WIN32)
	HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_SCREEN_BUFFER_INFO csbBefore;
	GetConsoleScreenBufferInfo(hstdout, &csbBefore);
	SetConsoleTextAttribute(hstdout, FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN | BACKGROUND_INTENSITY);
	printf("[Verbose]: ");

	SetConsoleTextAttribute(hstdout, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);
	printf("%s\n", buf);

	SetConsoleTextAttribute(hstdout, csbBefore.wAttributes);
#else
	printf("[INFO]: %s\n", buf);
#endif
	WriteToLogFile("[VERBOSE:] " + std::string(buf));
#endif
}

void OutputWarning(const char* warning, ...)
{
	char buf[512];
	va_list begin;

	va_start(begin, warning);
	vsnprintf(buf, 512, warning, begin);
	va_end(begin);

#if defined(WIN32) || defined(_WIN32)
	HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_SCREEN_BUFFER_INFO csbBefore;
	GetConsoleScreenBufferInfo(hstdout, &csbBefore);
	SetConsoleTextAttribute(hstdout, FOREGROUND_RED | FOREGROUND_GREEN);
	printf("[WARNING]: ");

	SetConsoleTextAttribute(hstdout, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);
	printf("%s\n", buf);

	SetConsoleTextAttribute(hstdout, csbBefore.wAttributes);
#else
	printf("[WARNING]: %s\n", buf);
#endif
	WriteToLogFile("[WARNING:] " + std::string(buf));
}

void OutputBugReport(const char* warning, ...)
{
	char buf[512];
	va_list begin;

	va_start(begin, warning);
	vsnprintf(buf, 512, warning, begin);
	va_end(begin);

#if defined(WIN32) || defined(_WIN32)
	HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_SCREEN_BUFFER_INFO csbBefore;
	GetConsoleScreenBufferInfo(hstdout, &csbBefore);
	SetConsoleTextAttribute(hstdout, FOREGROUND_RED | FOREGROUND_GREEN);
	printf("[BUG REPORT]: ");

	SetConsoleTextAttribute(hstdout, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);
	printf("%s\n", buf);

	SetConsoleTextAttribute(hstdout, csbBefore.wAttributes);
#else
	printf("[BUG REPORT]: %s\n", buf);
#endif
	WriteToBugReportFile("[BUG REPORT:] " + std::string(buf));
}

std::string GetErrorMessage(vcmpError errorId)
{

	switch (errorId)
	{
		case vcmpError::vcmpErrorNoSuchEntity:
			return std::string("No such entity defined!");

		case vcmpError::vcmpErrorBufferTooSmall:
			return std::string("Buffer too small!");

		case vcmpError::vcmpErrorTooLargeInput:
			return std::string("Too large input!");


		case vcmpError::vcmpErrorArgumentOutOfBounds:
			return std::string("Argument is out of bounds!");

		case vcmpError::vcmpErrorNullArgument:
			return std::string("Argument is null!");

		case vcmpError::vcmpErrorPoolExhausted:
			return std::string("Pool is exhausted!");

		case vcmpError::vcmpErrorInvalidName:
			return std::string("Invalid name specified!");

		case vcmpError::vcmpErrorRequestDenied:
			return std::string("Request was denied!");

		default:
			return std::string("No known error was found.");

	}
}
