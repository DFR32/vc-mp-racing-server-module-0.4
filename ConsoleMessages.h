#pragma once
#include "plugin.h"
#include <string>

void OutputMessage(const char* msg, ...);
void OutputInfo(const char* msg, ...);
void OutputWarning(const char* msg, ...);
void OutputError(const char* error, ...);
void OutputVerboseInfo(const char* msg, ...);
void OutputBugReport(const char* warning, ...);
std::string GetErrorMessage(vcmpError errorId);
