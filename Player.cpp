#include "Player.h"
#include "ConsoleMessages.h"
#include <cstdio>
#include <cstring>
#include <cstdarg>

extern PluginFuncs * gFuncs;

extern void processExternPart(unsigned int player);

Player::~Player()
{
	//OutputInfo("Called ~Player deconstructor.");
	//this->Deinitialize();
}

// Function for checking the validity of a player
bool Player::IsValid()
{
	return ((this->nPlayerId <= 100) && this->b_Initialized);
}

bool Player::IsSpawned()
{
	if (this->b_Initialized)
		return (gFuncs->IsPlayerSpawned(this->nPlayerId) == 1) ? true : false;

	return false;
}

bool Player::IsAway()
{
	if (this->b_Initialized)
		return (gFuncs->IsPlayerAway(this->nPlayerId) == 1) ? true : false;

	return false;
}

// Function for initializing the player upon joining
// Sets the initialization flag and the id
void Player::Initialize(int playerId)
{
	this->nPlayerId = playerId;
	this->b_Initialized = true;

	char name[64];
	gFuncs->GetPlayerName(playerId, name, 64);

	std::string strName(name);
	this->playerName = std::move(strName);
}


// Function for deinitializing the player. Called at disconnection
// Clears the class and allows it to be reused
void Player::Deinitialize()
{
	::processExternPart(this->nPlayerId);
	this->nPlayerId = 101;
	this->b_Initialized = false;

	this->playerName.clear();
	this->position.Reset();
	this->rotationEuler.Reset();
}

// Function for returning the player ID
int32_t Player::GetPlayerID()
{
	// Return the player id if the player is initialized
	// Return -1 if not
	if(this->b_Initialized)
		return this->nPlayerId;

	return -1;
}

int32_t Player::GetPlayerWeapon()
{
	// Return the player weapon if the player is initialized
	// Return -1 if not
	if (this->b_Initialized)
		return gFuncs->GetPlayerWeapon(this->nPlayerId);

	return -1;
}

// Function for returning the player name
std::string Player::GetPlayerName()
{
	// Return the player name
	return this->playerName;
}

std::string Player::GetCurrentPlayerName()
{
	char name[24];
	gFuncs->GetPlayerName(this->nPlayerId, name, 24);

	return std::string(name);
}

std::string Player::GetPlayerIP()
{
	if (!this->b_Initialized)
		return std::string("");

	char buffer[17];
	vcmpError error = gFuncs->GetPlayerIP(this->nPlayerId, buffer, 17);
	if (error)
	{
		OutputError("Error in Player::GetPlayerIP. %s error: %d", GetErrorMessage(error).c_str(), error);
		return std::string("");
	}

	return std::string(buffer);
}
// Function for returning the player position
Vector3 Player::GetPlayerPosition()
{
	// Check if player is initalized. If not, raise an error
	if (!this->b_Initialized)
		OutputError("Error in Player::GetPlayerPosition. Position is not defined for a non-initialized player.");

	// Declare locals for getting the position
	// Get the position from the server, make it into a Vector3 and return it.
	float fX, fY, fZ;
	gFuncs->GetPlayerPosition(this->nPlayerId, &fX, &fY, &fZ);

	return Vector3(fX, fY, fZ);
}

void Player::SetPlayerPosition(Vector3 position)
{
	if (!this->b_Initialized)
	{
		OutputError("Error in Player::SetPlayerPosition. Non-initialized player.");
		return;
	}

	gFuncs->SetPlayerPosition(this->nPlayerId, position.x, position.y, position.z);
}

// Function for returning the player rotation
float Player::GetPlayerHeading()
{
	// Check if player is initalized. If not, return null
	if (!this->b_Initialized)
	{
		OutputError("Error in Player::GetPlayerHeading. Heading is not defined for a non-initialized player.");
		return 0.0f;
	}

	// Declare locals for getting the position
	// Get the position from the server, make it into a Vector3 and return it.
	return gFuncs->GetPlayerHeading(this->nPlayerId);
}

// Function for sending messages to the player
vcmpError Player::SendPlayerMessage(uint32_t colour, const char* format, ...)
{
	char buf[512];
	va_list begin;

	va_start(begin, format);
	vsnprintf(buf, 512, format, begin);
	va_end(begin);

	vcmpError error = gFuncs->SendClientMessage(this->nPlayerId, colour, "%s", buf);
	if (error)
		OutputError("Error in Player::SendPlayerMessage [%s]", GetErrorMessage(error).c_str());

	return error;
}

// Function for sending game messages to the player
vcmpError Player::SendPlayerGameMessage(int32_t type, const char* format, ...)
{
	char buf[256];
	va_list begin;

	va_start(begin, format);
	vsnprintf(buf, 256, format, begin);
	va_end(begin);

	vcmpError error = gFuncs->SendGameMessage(this->nPlayerId, type, "%s", buf);
	if (error)
		OutputError("Error in Player::SendGameMessage [%s]", GetErrorMessage(error).c_str());

	return error;
}

vcmpError Player::GivePlayerWeapon( int32_t weaponId, int32_t ammo)
{
	// Make sure ped is initialized, return an error otherwise
	if (!this->b_Initialized)
		return vcmpError::vcmpErrorNoSuchEntity;

	// Player is initialized, give him the weapon
	vcmpError error = gFuncs->GivePlayerWeapon(this->nPlayerId, weaponId, ammo);

	if (error)
		OutputError("Error in Player::GivePlayerWeapon [%s]", GetErrorMessage(error).c_str());

	return error;
}

vcmpError Player::SetPlayerWeapon(int32_t weaponId, int32_t ammo)
{
	// Make sure ped is initialized, return an error otherwise
	if (!this->b_Initialized)
		return vcmpError::vcmpErrorNoSuchEntity;

	// Player is initialized, set his weapon
	vcmpError error = gFuncs->SetPlayerWeapon(this->nPlayerId, weaponId, ammo);

	if (error)
		OutputError("Error in Player::SetPlayerWeapon [%s]", GetErrorMessage(error).c_str());

	return error;
}

vcmpError Player::PutPlayerInVehicle(int32_t vehicleId, int32_t slotIndex, uint8_t makeRoom, uint8_t warp)
{
	if (!this->b_Initialized)
	{
		OutputError("Error in Player::PutPlayerInVehicle. Invalid player entity.");
		return vcmpError::vcmpErrorNoSuchEntity;
	}

	vcmpError error = gFuncs->PutPlayerInVehicle(this->nPlayerId, vehicleId, slotIndex, makeRoom, warp);
	if (error)
	{
		OutputError("Error in Player::PutPlayerInVehicle. %s [%d]", GetErrorMessage(error).c_str(), static_cast<int>(error));
	}

	return error;
}

vcmpError Player::SetPlayerOption(vcmpPlayerOption playerOption, uint8_t toggle)
{
	// Make sure ped is initialized, return an error otherwise
	if (!this->b_Initialized)
		return vcmpError::vcmpErrorNoSuchEntity;

	// Player is initialized, set his world
	vcmpError error = gFuncs->SetPlayerOption(this->nPlayerId, playerOption, toggle);
	if (error)
		OutputError("Error in Player::SetPlayerOption. [%s]", GetErrorMessage(error).c_str());

	return error;
}

vcmpError Player::SetPlayerWorld(int32_t world)
{
	// Make sure ped is initialized, return an error otherwise
	if (!this->b_Initialized)
		return vcmpError::vcmpErrorNoSuchEntity;

	// Player is initialized, set his world
	vcmpError error = gFuncs->SetPlayerWorld(this->nPlayerId, world);
	if (error)
		OutputError("Error in Player::SetPlayerWorld. [%s]", GetErrorMessage(error).c_str());

	return error;
}

vcmpError Player::SetImmune(bool immune)
{
	if (!this->b_Initialized)
	{
		OutputError("Error in Player::SetImmune. Cannot set immune to invalid player.");
		return vcmpError::vcmpErrorNoSuchEntity;
	}

	vcmpError error = gFuncs->SetPlayerImmunityFlags(this->nPlayerId, UINT32_MAX);
	if (error)
		OutputError("Error in Player::SetImmune. %s", GetErrorMessage(error).c_str());

	return error;
}

vcmpError Player::SendClientScriptData(const char* data, ...)
{
	// Make sure ped is initialized, return -1 otherwise
	if (!this->b_Initialized)
		return vcmpError::vcmpErrorNoSuchEntity;

	char totalBuf[132];
	char buf[128];

	va_list begin;

	va_start(begin, data);
	std::vsnprintf(buf, 128, data, begin);
	va_end(begin);

	unsigned short length = strlen(buf);
	unsigned short size = ((length >> 8) & 0xFF) | ((length & 0xFF) << 8);

	memcpy(totalBuf, &size, sizeof(size));
	memcpy(totalBuf + sizeof(size), buf, length);

	vcmpError error = gFuncs->SendClientScriptData(this->nPlayerId, totalBuf, sizeof(size) + length);
	if (error)
		OutputError("Error in Player::SendClientScriptData. %s", GetErrorMessage(error).c_str());

	return error;
}

int32_t Player::GetPlayerVehicleID()
{
	// Make sure ped is initialized, return -1 otherwise
	if (!this->b_Initialized)
		return -1;

	int32_t error = gFuncs->GetPlayerVehicleId(this->nPlayerId);
	if (error < 0)
	{
		OutputError("Error in Player::GetPlayerVehicleID. %s", GetErrorMessage(gFuncs->GetLastError()).c_str());
		return -1;
	}

	return error;
}

int32_t Player::GetPlayerWorld()
{
	// Make sure ped is initialized, return -1 otherwise
	if (!this->b_Initialized)
		return -1;

	return gFuncs->GetPlayerWorld(this->nPlayerId);
}

std::string Player::GetPlayerUID()
{
	if (!this->b_Initialized)
		return std::string("");

	char buffer[64];
	vcmpError error = gFuncs->GetPlayerUID(this->nPlayerId, buffer, 64);
	if (error)
	{
		OutputError("Error in Player::GetPlayerUID. %s error: %d", GetErrorMessage(error).c_str(), error);
		return std::string("");
	}

	return std::string(buffer);
}

std::string Player::GetPlayerUID2()
{
	if (!this->b_Initialized)
		return std::string("");

	char buffer[64];
	vcmpError error = gFuncs->GetPlayerUID2(this->nPlayerId, buffer, 64);
	if (error)
	{
		OutputError("Error in Player::GetPlayerUID2. %s error: %d", GetErrorMessage(error).c_str(), error);
		return std::string("");
	}

	return std::string(buffer);
}
