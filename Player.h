#pragma once
#include "plugin.h"
#include "Vector3.h"


class Player
{
	friend class PlayerPool;

private:
	int32_t nPlayerId;
	std::string playerName;
	bool b_Initialized;

public:
	Vector3 position;
	Vector3 rotationEuler;

	Player() :
		nPlayerId(101),
		playerName(""),
		b_Initialized(false),
		position(),
		rotationEuler()
	{}



	~Player();

	void Initialize(int playerId);
	void Deinitialize();

	bool IsValid();
	bool IsSpawned();
	bool IsAway();

	int32_t GetPlayerID();
	int32_t GetPlayerWeapon();

	std::string GetPlayerName();
	std::string GetCurrentPlayerName();
	std::string GetPlayerIP();
	std::string GetPlayerUID();
	std::string GetPlayerUID2();

	Vector3 GetPlayerPosition();
	void SetPlayerPosition(Vector3 position);

	float GetPlayerHeading();

	vcmpError SendPlayerMessage(uint32_t colour, const char* format, ...);
	vcmpError SendPlayerGameMessage(int32_t type, const char* format, ...);

	vcmpError GivePlayerWeapon(int32_t weaponId, int32_t ammo);
	vcmpError SetPlayerWeapon(int32_t weaponId, int32_t ammo);

	vcmpError PutPlayerInVehicle (int32_t vehicleId, int32_t slotIndex, uint8_t makeRoom, uint8_t warp);
	vcmpError SetPlayerOption(vcmpPlayerOption playerOption, uint8_t toggle);

	vcmpError SetPlayerWorld (int32_t world);

	vcmpError SetImmune(bool immune);
	vcmpError SendClientScriptData(const char* data, ...);
	int32_t GetPlayerVehicleID();
	int32_t GetPlayerWorld ();
};
