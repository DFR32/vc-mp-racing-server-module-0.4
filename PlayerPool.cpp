#include "PlayerPool.h"
#include "Player.h"
#include "ConsoleMessages.h"
#include "CommandHandler.h"
#include "Utils.h"

extern CommandHandler globalCommandHandler;


PlayerPool::~PlayerPool()
{

}

// Function which prepares the player class for connecting
void PlayerPool::ProcessJoin(unsigned int PlayerId)
{
	// Just some basic error checking
	// First, make sure we have a clean instance
	if (!this->Clean(PlayerId)) this->m_vPlayerPool.at(PlayerId).Deinitialize();

	// Now initialize it using the id provided by the server
	this->m_vPlayerPool.at(PlayerId).Initialize(PlayerId);

	// Our job is done
}

// Function which prepares the player class for reuse upon disconnecting
void PlayerPool::ProcessPart(unsigned int PlayerId)
{
	// Prepare the class for another player
	this->m_vPlayerPool.at(PlayerId).Deinitialize();

	// Our job is done
}

void PlayerPool::ProcessCommand(unsigned int PlayerId, std::string command)
{
	globalCommandHandler.Invoke(PlayerId, command);
}

void PlayerPool::ProcessCommand(unsigned int PlayerId, std::string command, std::string arguments)
{
	globalCommandHandler.Invoke(PlayerId, command, arguments);
}



// Function for establishing whether player class at index 'idx' is clean or not
bool PlayerPool::Clean(unsigned int idx)
{
	// Our job is to check if player slot 'idx' is initialized or not
	return !this->m_vPlayerPool.at(idx).b_Initialized;
}

// Function for establishing whether player class at index 'idx' is valid or not
bool PlayerPool::Valid(unsigned int idx)
{
	// Our job is to check if player slot 'idx' is valid or not
	return this->m_vPlayerPool.at(idx).IsValid();
}

bool PlayerPool::Valid(Player player)
{
	// Call the IsValid function to let the caller know whether player instance is valid or not
	return player.IsValid();
}


// Function for finding players with the exact id
Player PlayerPool::GetPlayer(unsigned int PlayerId)
{
	// Check if the playerid is within the vector's bounds
	if (this->Valid(PlayerId))
		return this->m_vPlayerPool.at(PlayerId);

	// If not, return an invalid player instance.
	return invalidPlayer;
}

// Function for finding players with the exact name
Player PlayerPool::GetPlayer(std::string targetPlayerName)
{
	// Do a clean itteration through the entire player pool
	// Return the player with the given name
	for (auto & player : this->m_vPlayerPool)
	{
		if (!player.IsValid()) continue;

		if (player.playerName == targetPlayerName) return player;
	}

	// If not, return an invalid player instance.
	return invalidPlayer;
}

// Function for finding players with a certain tag
Player PlayerPool::GetPlayerWithTag(std::string PlayerTag)
{
	std::string loweredTag = Utils::LowerStr(PlayerTag);
	// Do a clean itteration through the entire player pool
	// Return the player whose name contains the given tag
	for (auto & player : this->m_vPlayerPool)
	{
		if (!player.IsValid()) continue;


		if (Utils::LowerStr(player.playerName).find(loweredTag) != std::string::npos)
		{
			OutputVerboseInfo("Found player with tag [%s] in the playerpool.", PlayerTag.c_str());
			return player;
		}

	}
	// If not, return an invalid player instance.
	return invalidPlayer;
}

size_t PlayerPool::GetSize()
{
	return this->m_vPlayerPool.size();
}

size_t PlayerPool::GetPlayerCount()
{
	size_t playerCount = 0;
	for (auto & player : this->m_vPlayerPool)
		if (player.IsValid())
			++playerCount;

	return playerCount;
}
