#pragma once
#include "Player.h"
#include "Vector3.h"
#include <vector>

#define MAX_PLAYERS 100

class PlayerPool
{
private:
	std::vector<Player>	m_vPlayerPool;
	Player invalidPlayer;

public:
	PlayerPool()
		: m_vPlayerPool(100, Player())
	{
	}


	~PlayerPool();

	void ProcessJoin(unsigned int PlayerId);
	void ProcessPart(unsigned int PlayerId);
	void ProcessCommand(unsigned int PlayerId, std::string command);
	void ProcessCommand(unsigned int PlayerId, std::string command, std::string arguments);

	Player GetPlayer(unsigned int PlayerId);
	Player GetPlayer(std::string targetPlayerName);
	Player GetPlayerWithTag(std::string PlayerTag);

	size_t GetSize();
	size_t GetPlayerCount();

	bool Clean(unsigned int idx);
	bool Valid(unsigned int idx);
	bool Valid(Player player);
};
