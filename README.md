# README #

This is a C++ plugin for programming gamemodes for VCMP. It's in WIP stage.

### Information ###

* Version: 0.0.1

### Contributions ###

* SLC

### Benchmarked commands (using benchmark) ###

* /findplayer (using "abcdefghijklmnopqrstuv2")

![console_result.png](https://bitbucket.org/repo/AqK6od/images/2872173075-console_result.png)