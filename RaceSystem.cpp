#include "RaceSystem.h"
#include "VehiclePool.h"
#include "PlayerPool.h"
#include "ConsoleMessages.h"
#include "World.h"
#include "Utils.h"
#include "Server.h"
#include "TimerHandler.h"
#include <cmath>
#include <inttypes.h>

extern VehiclePool globalVehiclePool;
extern PlayerPool globalPlayerPool;
extern World globalWorld;
extern Server globalServer;
extern TimerHandler globalTimerHandler;
extern PluginFuncs * gFuncs;

extern void externStartFunction();
extern void externEndFunction();

void RaceSystem::UpdateTrackList()
{
	unsigned int _trackCount = 0;
	char buf[64];
	for (int i = 1; i < MAX_TRACKS; ++i)
	{
		snprintf(buf, 64, "Tracks/track%u.xml", i);

		if (!Utils::IsFileReadable(buf))
			break;

		++_trackCount;
	}
	this->trackCount = _trackCount;

	OutputInfo("%u tracks available for loading.", this->trackCount);
}

void RaceSystem::MoveToLastCP(Player player)
{
	if (!this->b_raceInProgress)
	{
        OutputVerboseInfo("RaceSystem::MoveToLastCP: No race is in progress. Return");
		return;
    }

	unsigned int playerCheckpointsCrossed = static_cast<unsigned int>(this->playerCheckpoints.at(player.GetPlayerID()));
	if (playerCheckpointsCrossed >= (this->GetCheckpointCount() - 1))
    {
        OutputVerboseInfo("RaceSystem::MoveToLastCP: Checkpoints crossed >= checkpoint count. %u %u", playerCheckpointsCrossed, this->GetCheckpointCount() - 1);
		return;
    }

	Vehicle vehicle = globalVehiclePool.GetVehicle(player.GetPlayerVehicleID()-1);
	if (!vehicle.IsValid())
    {
        OutputVerboseInfo("RaceSystem::MoveToLastCP: Vehicle is invalid");
		return;
    }

	RaceCheckpointStruct tmpCheckpoint = this->trackCheckpoints.at(playerCheckpointsCrossed - 1);
	vehicle.SetPosition(tmpCheckpoint.position, 0);
	vehicle.SetVehicleRotationEuler(Vector3(0, 0, tmpCheckpoint.heading));
}

void RaceSystem::Clean(unsigned int nPlayerId)
{
	std::pair<Player, SphereCheckpoint> cpPair = this->worldCheckpoints[nPlayerId];
	SphereCheckpoint cp = cpPair.second;

	if (cp.IsValid())
	{
		OutputMessage("Deleted CP because it was valid.");
		cp.Delete();
	} else OutputMessage("Could not delete CP because it was valid.");

	this->playerCheckpoints.at(nPlayerId) = -1;
    this->playerLaps.at(nPlayerId) = 1;

	Player player = globalPlayerPool.GetPlayer(nPlayerId);
	Vehicle vehicle = this->playerVehicles.at(nPlayerId);
	unsigned int vID = vehicle.GetVehicleID();

	OutputVerboseInfo("Vehicle IDs: %u %u %u %u", vID, vID + 1, vID - 1, nPlayerId);
	globalVehiclePool.Destroy(vID - 1);
	this->playerVehicles.at(nPlayerId) = Vehicle();
}

int RaceSystem::CreateCarForPlayer(Player player, bool b_atSpawn)
{
	if (!this->b_raceInProgress)
	{
		OutputInfo("RaceSystem::CreateCarForPlayer: Race has not started. Returning..");
		return -1;
	}

	float margin = 7.0 * globalPlayerPool.GetPlayerCount();
	int nVehicleId;
	if (this->_vehicleModels.empty())
	{
		OutputInfo("RaceSystem::CreateCarForPlayer: Vehicle models is empty, process using one model #1..");
		if (b_atSpawn)
		{
			OutputInfo("RaceSystem::CreateCarForPlayer: Process car deployment at spawn #1..");
			float carHeading = this->trackCheckpoints.at(0).heading;
			nVehicleId = globalVehiclePool.Add(this->_vehicleModel, 1, Utils::GetBackPosition(this->trackCheckpoints.at(0).position, carHeading, margin), carHeading, rand() % 40, rand() % 40);
			this->playerVehicles.at(player.GetPlayerID()) = globalVehiclePool.GetVehicle(nVehicleId);
			this->PrepareCheckpoints(player);
			OutputInfo("Created car for player in pool slot %u. #1", nVehicleId);
		}
		else
		{
			OutputInfo("RaceSystem::CreateCarForPlayer: Process car deployment at last checkpoint #1..");
			int playerLastCP = this->playerCheckpoints.at(player.GetPlayerID()) - 1;
			float carHeading = this->trackCheckpoints.at(playerLastCP).heading;
			nVehicleId = globalVehiclePool.Add(this->_vehicleModel, 1, Utils::GetBackPosition(this->trackCheckpoints.at(playerLastCP).position, carHeading, margin), carHeading, rand() % 40, rand() % 40);
			this->playerVehicles.at(player.GetPlayerID()) = globalVehiclePool.GetVehicle(nVehicleId);
			OutputInfo("Created car for player in pool slot %u. #1", nVehicleId);
		}
	}
	else
	{
		OutputInfo("RaceSystem::CreateCarForPlayer: Vehicle models is filled, process using model array #2..");
		std::vector<std::string> splitArguments;
		splitArguments = Utils::splitString(this->_vehicleModels, ',');
		int32_t carArraySize = static_cast<int32_t>(splitArguments.size());

		if (b_atSpawn)
		{
			OutputInfo("RaceSystem::CreateCarForPlayer: Process car deployment at spawn #2..");
			float carHeading = this->trackCheckpoints.at(0).heading;
			nVehicleId = globalVehiclePool.Add(atoi((splitArguments.at(rand() % carArraySize).c_str())), 1, Utils::GetBackPosition(this->trackCheckpoints.at(0).position, carHeading, margin), carHeading, rand() % 40, rand() % 40);
			this->playerVehicles.at(player.GetPlayerID()) = globalVehiclePool.GetVehicle(nVehicleId);
			this->PrepareCheckpoints(player);
			OutputInfo("Created car for player in pool slot %u. #2", nVehicleId);
		}
		else
		{
			OutputInfo("RaceSystem::CreateCarForPlayer: Process car deployment at last checkpoint #2..");
			int playerLastCP = this->playerCheckpoints.at(player.GetPlayerID()) - 1;
			float carHeading = this->trackCheckpoints.at(playerLastCP).heading;
			nVehicleId = globalVehiclePool.Add(atoi((splitArguments.at(rand() % carArraySize).c_str())), 1, Utils::GetBackPosition(this->trackCheckpoints.at(playerLastCP).position, carHeading, margin), carHeading, rand() % 40, rand() % 40);
			this->playerVehicles.at(player.GetPlayerID()) = globalVehiclePool.GetVehicle(nVehicleId);
			OutputInfo("Created car for player in pool slot %u. #2", nVehicleId);
		}
	}

	player.PutPlayerInVehicle(nVehicleId + 1, 0, 1, 1);

	Vehicle tmpVehicle = globalVehiclePool.GetVehicle(nVehicleId);

	if(tmpVehicle.IsValid())
	{
        OutputInfo("Made vehicle %u immune.", nVehicleId);
		tmpVehicle.SetImmune(true);
    } else  OutputError("Could not make vehicle %u immune.", nVehicleId);
	OutputInfo("Attempting to put player [%d] in vehicle [%d]", player.GetPlayerID(), nVehicleId + 1);

	for (int i = 0; i < 100; ++i)
		if (globalVehiclePool.GetVehicle(i).IsValid())
			OutputInfo("Vehicle in pool by ID: [%u] and VCMP Pool: [%u]", i, i + 1);

	player.SetImmune(true);
	return nVehicleId;
}

unsigned int RaceSystem::GetTrackCount()
{
	return this->trackCount;
}

void RaceSystem::CreateRaceTrack(std::string trackname, std::string author, int32_t model, float heading, float lateralgap, float backwardsgap)
{
	//if (this->createdTrack)
	//{
	//	OutputError("Cannot create another track");
	//}

	pugi::xml_node Track = this->createdTrack.append_child("Track");
	Track.append_attribute("TrackName") = trackname.c_str();
	Track.append_attribute("Author") = author.c_str();
	Track.append_attribute("Record") = "01:00:00.000";

	pugi::xml_node Settings = Track.append_child("Settings");
	Settings.append_attribute("VehicleModel") = model;
	Settings.append_attribute("Weather") = globalWorld.GetWeather();
	Settings.append_attribute("Hour") = globalWorld.GetHour();
	Settings.append_attribute("Minute") = globalWorld.GetMinute();
	Settings.append_attribute("Heading") = heading;
	Settings.append_attribute("LateralGap") = lateralgap;
	Settings.append_attribute("BackwardsGap") = backwardsgap;

	nodeCheckpoints = Settings.append_child("Checkpoints");

}

void RaceSystem::CreateCheckpoint(Vector3 checkpoint, float headingAngle)
{
	pugi::xml_node checkpointNode = this->nodeCheckpoints.append_child("Checkpoint");
	checkpointNode.append_attribute("X") = checkpoint.x;
	checkpointNode.append_attribute("Y") = checkpoint.y;
	checkpointNode.append_attribute("Z") = checkpoint.z;
	checkpointNode.append_attribute("Angle") = headingAngle;
}

void RaceSystem::SaveCreatedTrack(std::string path)
{
	if(this->createdTrack)
		this->createdTrack.save_file(path.c_str());
}

void RaceSystem::LoadRaceTrack(std::string file_path)
{
	if (this->b_spawnedGrid == true)
	{
		OutputError("Cannot load another track while a race is already prepared.");
		return;
	}

	pugi::xml_parse_result result = this->race_track.load_file(file_path.c_str());

	if (!result)
	{
		OutputError( "XML [\"%s\"] parsed with errors, attr value: [\"%s\"]", file_path.c_str(), this->race_track.child("node").attribute("attr").value());
		OutputError( "Error description: \"%s\"", result.description());
		OutputError("Error offset: \"%d\" (error at [...\"%u\"]", result, result.offset);
		return;
	}

	OutputInfo("XML [\"%s\"] parsed without errors. Loading nodes..", file_path.c_str());

	pugi::xml_node track = this->race_track.child("Track");

	if (!track)
	{
		OutputError("XML [\"%s\"] lacks node \"Track\". Loading failed..", file_path.c_str());
		return;
	}

	std::string TrackName("Unknown");
	std::string Author("None");

	if (track.attribute("TrackName"))
		TrackName = std::string(track.attribute("TrackName").value());

	if(track.attribute("Author"))
		Author = std::string(track.attribute("Author").value());


	unsigned int timeHour = 1, timeMinute = 0, timeSecond = 0, timeMicroseconds = 0;
	std::string trackRecord;
	if (track.attribute("Record"))
	{
		trackRecord = std::string(track.attribute("Record").value());

		sscanf(trackRecord.c_str(), "%u:%u:%u.%u", &timeHour, &timeMinute, &timeSecond, &timeMicroseconds);

		this->record = timeHour * 3600000 + timeMinute * 60000 + timeSecond * 1000 + timeMicroseconds;
	}
	else {
		trackRecord = std::string("Not Set");
		this->record = 3600000;
	}

	this->_authorName = Author;
	this->_trackName = TrackName;

	pugi::xml_node trackSettings = track.child("Settings");

	if (!trackSettings)
	{
		OutputError("XML [\"%s\"] lacks node \"Settings\". Loading failed..", file_path.c_str());
		return;
	}

	int32_t vehicleModel = 90;

	if (trackSettings.attribute("VehicleModel"))
	{
		vehicleModel = trackSettings.attribute("VehicleModel").as_int();
		this->_vehicleModel = vehicleModel;
	}
	else if (trackSettings.attribute("VehicleModels"))
	{
		this->_vehicleModels = trackSettings.attribute("VehicleModels").as_string();
		this->_vehicleModel = 0;
	}
	else this->_vehicleModel = 90;


	int32_t weather;
	if (trackSettings.attribute("Weather"))
		weather = trackSettings.attribute("Weather").as_int();
	else weather = 1;

	this->worldWeather = weather;

	int32_t hour;
	if (trackSettings.attribute("Hour"))
		hour = trackSettings.attribute("Hour").as_int();
	else hour = 12;

	this->worldHour = hour;

	int32_t minute;
	if (trackSettings.attribute("Minute"))
		minute = trackSettings.attribute("Minute").as_int();
	else minute = 0;

	this->worldMinute = minute;

	float vehicleHeading;
	if (trackSettings.attribute("Heading"))
		vehicleHeading = trackSettings.attribute("Heading").as_float();
	else vehicleHeading = 0.0f;

	this->heading = vehicleHeading;

	float lateralGap;
	if (trackSettings.attribute("LateralGap"))
		lateralGap = trackSettings.attribute("LateralGap").as_float();
	else lateralGap = 0.0f;

	this->startLateralGap = lateralGap;

	float backwardsGap;
	if (trackSettings.attribute("BackwardsGap"))
		backwardsGap = trackSettings.attribute("BackwardsGap").as_float();
	else backwardsGap = 0.0f;

	this->startBackwardsGap = backwardsGap;

	unsigned int numLaps;
	if(trackSettings.attribute("Laps"))
        numLaps = trackSettings.attribute("Laps").as_uint();
    else numLaps = 0;

    this->laps = numLaps;

	this->startBackwardsGap = backwardsGap;

	pugi::xml_node checkpoints = trackSettings.child("Checkpoints");

	if (!checkpoints)
	{
		OutputError("XML [\"%s\"] lacks node \"Checkpoints\". Loading failed..", file_path.c_str());
		return;
	}

	unsigned int checkpoint_count = 0;
	for (pugi::xml_node checkpoint = checkpoints.child("Checkpoint"); checkpoint; checkpoint = checkpoint.next_sibling("Checkpoint"))
	{
		if (!checkpoint) break;

		if (checkpoint_count == 998)
		{
			OutputError("XML [\"%s\"] has too much checkpoints. Stopped loading at %u checkpoints..", file_path.c_str(), checkpoint_count);
			break;
		}

		if (checkpoint.attribute("X") && checkpoint.attribute("Y") && checkpoint.attribute("Z"))
			this->trackCheckpoints[checkpoint_count].position =	Vector3(
					checkpoint.attribute("X").as_float(),
					checkpoint.attribute("Y").as_float(),
					checkpoint.attribute("Z").as_float()
					);

		if (checkpoint.attribute("Angle"))
			this->trackCheckpoints[checkpoint_count].heading = checkpoint.attribute("Angle").as_float();
		else this->trackCheckpoints[checkpoint_count].heading = this->heading;


		++checkpoint_count;
	}

	OutputInfo("Loaded XML map [\"%s\"] by [%s] with %u checkpoints.", file_path.c_str(), Author.c_str(), checkpoint_count);

	globalServer.SendMessage(Utils::RGBAtoHEX(30, 255, 30, 225), "--------------------------------------------------------------------------");
	globalServer.SendMessage(Utils::RGBAtoHEX(225, 25, 225, 225), "Loaded race track [%s] by [%s] with %u checkpoints.", this->_trackName.c_str(), Author.c_str(), checkpoint_count);
	globalServer.SendMessage(Utils::RGBAtoHEX(225, 25, 225, 225), "Record is [%s]", trackRecord.c_str());

	this->b_loadedRaceTrack = true;
	this->_checkpointCount = checkpoint_count;

	globalWorld.SetWeather(this->worldWeather);
	globalWorld.SetHour(this->worldHour);
	globalWorld.SetMinute(this->worldMinute);
	CreateRightRaceGrid(1, this->heading, this->trackCheckpoints.at(0).position, static_cast<unsigned int>(this->GetEligiblePlayerCount()), this->startLateralGap, this->startBackwardsGap);
	OutputInfo("Creating race grid with arguments [Model: %u World: 1 Heading: %.3f Start: [%s] Vehicle Count: %u Lateral Gap: %.3f Backwards Gap: %.3f", this->_vehicleModel, this->heading,
		static_cast<std::string>(this->trackCheckpoints.at(0).position).c_str(), globalPlayerPool.GetPlayerCount(), this->startLateralGap, this->startBackwardsGap);

	this->path = file_path;
}

void RaceSystem::CreateRightRaceGrid(int32_t world, float heading, Vector3 startPosition, unsigned int numberOfVehicles, float leftGap, float backwardsGap)
{
	bool useMultipleCars = false;
	int32_t carArraySize = -1;
	std::vector<std::string> splitArguments;

	if (this->_vehicleModel == 0 && !this->_vehicleModels.empty())
	{
		splitArguments = Utils::splitString(this->_vehicleModels, ',');
		useMultipleCars = true;
		carArraySize = static_cast<int32_t>(splitArguments.size());
	}

	bool toggleLights = false;
	if ((this->worldHour >= 0 && this->worldHour <= 6) || (this->worldHour >= 19 && this->worldHour <= 23))
		toggleLights = true;

	//unsigned int timeSeed = static_cast<unsigned int>(time(NULL));
	int32_t vehicleId;
	Vector3 backupStartPosition = startPosition;

	Vehicle tmpVehicle;

	// Compute left position
	Vector3 leftPosition = Vector3(startPosition.x + leftGap * std::cos(heading) + 1.0f * std::sin(heading), startPosition.y - leftGap * std::sin(heading) + 1.0f * std::cos(heading), startPosition.z);

	bool left;
	srand(static_cast<unsigned int>(time(NULL)));
	for (unsigned int i = 0; i < numberOfVehicles; ++i)
	{
		left = (i % 2 == 1);

		if (left)
			startPosition = leftPosition;
		else
			startPosition = backupStartPosition;

			startPosition = Vector3(startPosition.x + i * backwardsGap * std::sin(heading), startPosition.y - i * backwardsGap * std::cos(heading), startPosition.z);

			if (!useMultipleCars)
				vehicleId = globalVehiclePool.Add(this->_vehicleModel, world, startPosition, heading, rand() % 40, rand() % 40);
			else
				vehicleId = globalVehiclePool.Add(std::atoi(splitArguments.at(rand() % carArraySize).c_str()), world, startPosition, heading, rand() % 40, rand() % 40);

			tmpVehicle = globalVehiclePool.GetVehicle(vehicleId);
			if (toggleLights)
				tmpVehicle.SetLights(true);
			tmpVehicle.SetImmune(true);

			Player player;
			for (unsigned int k = 0; k < 100; ++k)
			{
				player = globalPlayerPool.GetPlayer(k);
				if (!player.IsValid())
					continue;

				if(!this->playerVehicles.at(k).IsValid())
					this->playerVehicles.at(player.GetPlayerID()) = globalVehiclePool.GetVehicle(vehicleId);
			}
	}
	this->b_spawnedGrid = true;

	this->PutPlayersInVehicles();
}

void RaceSystem::PutPlayersInVehicles()
{
	Player player;
    Vehicle vehicle;

	for (unsigned int i = 0; i < 100; ++i)
	{
		player = globalPlayerPool.GetPlayer(i);

		if (!player.IsValid())
		{
			OutputVerboseInfo("Invalid player %u. skipping...", i);
			continue;
		}

		for (unsigned k = 0; k < 1000; ++k)
		{
            vehicle = globalVehiclePool.GetVehicle(k);
			if (!vehicle.IsValid())
			{
				OutputVerboseInfo("Invalid vehicle %u %p. skipping...", k, &vehicle);
				continue;
			}

			if (vehicle.IsSeatOccupied(0) != 255)
			{
				OutputVerboseInfo("Occupied vehicle %u %p. skipping...", k, &vehicle);
				continue;
			}


			player.PutPlayerInVehicle(k+1, 0, 1, 1);
			OutputVerboseInfo("Put player [%d] in car [%d].", i, k+1);
			OutputVerboseInfo("Difference: %u %u", k, vehicle.GetVehicleID());
			this->PrepareCheckpoints(player);
			player.SetPlayerOption(vcmpPlayerOption::vcmpPlayerOptionControllable, 0);
			player.SetImmune(true);
			//vehicle.SetDoorsLocked(true);
			break;
		}
	}
	externStartFunction();
	b_readyToStart = true;
}

void RaceSystem::StartRaceCountdown()
{
}

void RaceSystem::PrepareCheckpoints(Player player)
{
	if (!player.IsValid())
	{
		OutputError("Error in RaceSystem::PrepareCheckpoints. Player instance is invalid.");
		return;
	}

	if (!this->trackCheckpoints.at(1).position.IsInitialised())
	{
		OutputError("Error in RaceSystem::PrepareCheckpoints. Not enough checkpoints to generate the checkpoint map.");
		return;
	}

	SphereCheckpoint sphereCP;
	Vector3 cpPosition = this->trackCheckpoints.at(1).position;

	sphereCP.CreateCheckpoint(player, 1, 0, cpPosition, RGBA(225, 40, 200, 225), 3.0f, this->trackCheckpoints.at(1).heading);

	int nPlayerId = player.GetPlayerID();

	this->worldCheckpoints[nPlayerId] = std::pair<Player, SphereCheckpoint>(player, sphereCP);
	this->playerCheckpoints[nPlayerId] = 1;

	player.SendClientScriptData("C %.3f %.3f %.3f %.3f", cpPosition.x, cpPosition.y, cpPosition.z, this->trackCheckpoints.at(1).heading);
}


void RaceSystem::ProcessCheckpoint(Player player)
{
	int32_t playerID = player.GetPlayerID();
	unsigned int currentPlayerCheckpoint = this->playerCheckpoints[playerID];
	unsigned int currentPlayerLaps = this->playerLaps.at(playerID);
	SphereCheckpoint playerCheckpoint;

	playerCheckpoint = this->worldCheckpoints[playerID].second;
	if (!playerCheckpoint.IsValid())
	{
		OutputError("Error in RaceSystem::ProcessCheckpoint. Checkpoint associated with player became invalid.");
		return;
	}

	//player.SendPlayerMessage(Utils::RGBAtoHEX(225, 25, 225, 225), "Checkpoint [%u/%u] crossed.", currentPlayerCheckpoint, this->_checkpointCount - 1);
	if ((this->_checkpointCount - 1) == currentPlayerCheckpoint)
	{
        if(this->laps == 0)
        {
            playerCheckpoint.Delete();
            ++this->playerCheckpoints[playerID];
            this->ProcessFinish(player);
            OutputInfo("Player [%d %s] has finished the race.", playerID, player.GetPlayerName().c_str());
            return;
        }
        else {
            if(currentPlayerLaps == this->laps)
            {
                playerCheckpoint.Delete();
                ++this->playerCheckpoints[playerID];
                this->ProcessFinish(player);
                OutputInfo("Player [%d %s] has finished the race.", playerID, player.GetPlayerName().c_str());
                return;
            }
            else
            {
                OutputInfo("Processing lap %u for player %u. %u", currentPlayerLaps, playerID, this->laps);
                this->ProcessLap(player);
                currentPlayerCheckpoint = 1;
            }
        }
	}

	Vector3 cpPosition = this->trackCheckpoints.at(currentPlayerCheckpoint + 1).position;
	playerCheckpoint.SetCheckpointPosition(cpPosition, this->trackCheckpoints.at(currentPlayerCheckpoint + 1).heading);
	OutputInfo("Moving player's [%d %s] checkpoint to %s.", playerID, player.GetPlayerName().c_str(), static_cast<std::string>(cpPosition).c_str());
	++this->playerCheckpoints[playerID];
	OutputInfo("Increased player checkpoints to %d.", this->playerCheckpoints[playerID]);

	Vehicle playerVehicle = globalVehiclePool.GetVehicle(player.GetPlayerVehicleID()-1);

	player.SendClientScriptData("C %.3f %.3f %.3f %.3f", cpPosition.x, cpPosition.y, cpPosition.z, playerVehicle.GetVehicleRotationEuler().z);
	player.SendClientScriptData("U %u %u %u %u", currentPlayerCheckpoint, this->_checkpointCount - 1, currentPlayerLaps, this->laps);

	Player tmpItPlayer;
	unsigned int place = 1;

	for (unsigned int i = 0; i < 100; ++i)
	{
		tmpItPlayer = globalPlayerPool.GetPlayer(i);
		if (!tmpItPlayer.IsValid())
			continue;

		if ((this->playerCheckpoints[i] + ( 500 * this->playerLaps[i])) > (this->playerCheckpoints[playerID] + ( 500 * this->playerLaps[playerID])))
			++place;
	}
	player.SendClientScriptData("P %u", place);
}

void RaceSystem::StartRace()
{
	this->b_raceInProgress = true;
	this->timeStart = Utils::GetCurrentSysTime() / 1000;
}

void RaceSystem::EndRace()
{
	this->b_raceInProgress = false;
	this->b_readyToStart = false;
	this->b_spawnedGrid = false;
	this->b_loadedRaceTrack = false;

	this->heading = 0.0f;
	this->startBackwardsGap = 0.0f;
	this->startLateralGap = 0.0f;
	this->_authorName = "Unknown";
	this->_trackName = "Unknown";
	this->_vehicleModels.clear();
	this->_vehicleModel = 0;
	this->worldHour = 0;
	this->worldMinute = 0;
	this->worldWeather = 0;
	this->_checkpointCount = 0;
	this->playersFinished = 0;
	this->record = 3600;
	this->timeStart = 0;

	for (auto & checkpoint : this->worldCheckpoints)
	{
		checkpoint.first = Player();

		if (checkpoint.second.IsValid())
			checkpoint.second.Delete();
	}

	Vehicle vehicle;
	unsigned int amountOfVehicles = 0;
	for (unsigned int i = 0; i < 100; ++i)
	{
		vehicle = globalVehiclePool.GetVehicle(i);
		if (!vehicle.IsValid())
			continue;

		++amountOfVehicles;
	}
	OutputInfo("Amount of vehicles before: %u", amountOfVehicles);

	OutputInfo("Cleaning after race. PlayerVehicles size: %u", this->playerVehicles.size());

	for (unsigned int i = 0; i < 100; ++i)
	{
		vehicle = globalVehiclePool.GetVehicle(i);
		if (!vehicle.IsValid())
			continue;

		globalVehiclePool.Destroy(i);
	}
	OutputInfo("Cleaned after race. PlayerVehicles size: %u", this->playerVehicles.size());

	amountOfVehicles = 0;
	for (unsigned int i = 0; i < 100; ++i)
	{
		vehicle = globalVehiclePool.GetVehicle(i);
		if (!vehicle.IsValid())
			continue;

		++amountOfVehicles;
	}
	OutputInfo("Amount of vehicles left: %u", amountOfVehicles);

	for (auto & checkpoint : this->playerCheckpoints)
	{
		checkpoint = -1;
	}

	for(auto & pLaps : this->playerLaps)
	{
        pLaps = 1;
	}

	for (auto & trackCheckpoint : this->trackCheckpoints)
	{
		trackCheckpoint.position.Reset();
	}

	this->endTime = Utils::GetCurrentSysTime();
}

void RaceSystem::ProcessFinish(Player player)
{


	int64_t finishTime = Utils::GetCurrentSysTime() / 1000;
	unsigned int duration = static_cast<unsigned int>((finishTime - this->timeStart));
	unsigned int durationSec = duration / 1000;

	char buffer[16];
	std::snprintf(buffer, 16, "%.2u:%.2u:%.2u.%.3u", durationSec / 3600, durationSec % 3600 / 60, durationSec % 60, duration % 1000);

	if (this->playersFinished == 0)
	{
        OutputInfo("Duration: %u | Record: %u ", duration, this->record);
		externEndFunction();
		if (duration < this->record)
		{
			globalServer.SendMessage(Utils::RGBAtoHEX(225, 25, 225, 225), "New map record established by %s! [%s]", player.GetCurrentPlayerName().c_str(), buffer);
			this->UpdateRecord(std::string(buffer));
		}
	}

	player.SendPlayerMessage(Utils::RGBAtoHEX(225, 25, 225, 225), "Congratulations! You placed %s in %s with a time of %s !", Utils::GetPlaceStrFromNumber(this->playersFinished + 1).c_str(), this->_trackName.c_str(), buffer);
	++this->playersFinished;

	player.SendClientScriptData("F");
}

void RaceSystem::ProcessLap(Player player)
{
    int32_t playerID = player.GetPlayerID();
    this->playerCheckpoints[playerID] = 1;
    this->playerLaps.at(playerID)++;
}

void RaceSystem::Debug()
{
	if (!this->b_loadedRaceTrack)
		OutputError("Cannot output checkpoints info because no track was loaded");

	unsigned int checkpoint_count = 0;
	for (auto & checkpoint : this->trackCheckpoints)
	{
		if (!checkpoint.position.IsInitialised()) break;

		OutputInfo("Information about checkpoint [%u]: %s", checkpoint_count, static_cast<std::string>(checkpoint.position).c_str());
		++checkpoint_count;
	}
}

bool RaceSystem::ReadyToStart()
{
	return b_readyToStart;
}

bool RaceSystem::HasGridBeenSpawned()
{
	return this->b_spawnedGrid;
}

bool RaceSystem::HasStarted()
{
	return this->b_raceInProgress;
}

unsigned int RaceSystem::GetVehicleModel()
{
	if(this->b_loadedRaceTrack)
		return this->_vehicleModel;
	else return 0;
}

unsigned int RaceSystem::GetCheckpointCount()
{
	if (!this->b_loadedRaceTrack)
	{
		OutputError("Error in RaceSystem::GetCheckpointCount(). No race track was loaded.");
		return 0;
	}

	return this->_checkpointCount;
}

std::string RaceSystem::GetAuthor()
{
	if (this->b_loadedRaceTrack)
		return this->_authorName;
	return std::string("Unknown");
}

std::string RaceSystem::GetTrackName()
{
	if (this->b_loadedRaceTrack)
		return this->_trackName;
	return std::string("Unknown");
}

Vector3 RaceSystem::GetCheckpoint(unsigned int checkpoint)
{
	if (this->b_loadedRaceTrack)
		if (this->trackCheckpoints.size() < checkpoint)
			return this->trackCheckpoints.at(checkpoint).position;

	return Vector3();
}

int RaceSystem::GetPlayerCheckpointProgress(unsigned int nPlayerID)
{
	if (!this->b_raceInProgress)
	{
		OutputError("Error in RaceSystem::GetPlayerCheckpointProgress. No progress for player [%d] because no race is in progress.", nPlayerID);
		return 0;
	}

	return playerCheckpoints.at(nPlayerID);
}

std::pair<Player, SphereCheckpoint> RaceSystem::GetPlayerCheckpointData(unsigned int nPlayerID)
{
	if (!this->b_raceInProgress)
	{
		OutputError("Error in RaceSystem::GetPlayerCheckpointData. No checkpoint data for player [%d] because no race is in progress.", nPlayerID);
		return std::pair<Player, SphereCheckpoint>(Player(), SphereCheckpoint());
	}

	return worldCheckpoints.at(nPlayerID);
}

void RaceSystem::UpdateRecord(std::string record)
{
	if (!this->b_raceInProgress)
		return;

	pugi::xml_node track = this->race_track.child("Track");

	if (track.attribute("Record"))
	{
		track.remove_attribute("Record");
		track.append_attribute("Record") = record.c_str();
	}
	else track.append_attribute("Record") = record.c_str();
	this->race_track.save_file(this->path.c_str());
}

int64_t RaceSystem::GetEndTime()
{
	return this->endTime;
}

unsigned int RaceSystem::GetEligiblePlayerCount()
{
	Player player;
	unsigned int eligiblePlayerCount = 0;
	for (unsigned int i = 0; i < 100; ++i)
	{
		player = globalPlayerPool.GetPlayer(i);
		if (!player.IsValid())
			continue;

		if (!player.IsAway() && player.IsSpawned())
			++eligiblePlayerCount;
	}
	return eligiblePlayerCount;
}
