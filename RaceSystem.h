#pragma once
#include <stdint.h>
#include <vector>
#include "Player.h"
#include "Vehicle.h"
#include "Checkpoints.h"
#include "pugixml/pugixml.hpp"
#include <chrono>
#include <ctime>

#define MAX_TRACKS 1000
#define MAX_TRACK_CHECKPOINTS 1000

struct RaceCheckpointStruct
{
	Vector3 position;
	float heading;
};

class RaceSystem
{
private:
	bool b_raceInProgress;
	bool b_readyToStart;
	bool b_spawnedGrid;
	bool b_loadedRaceTrack;

	std::vector<RaceCheckpointStruct> trackCheckpoints;
	std::vector<std::pair<Player, SphereCheckpoint>> worldCheckpoints;
	std::vector<int> playerCheckpoints;
	std::vector<unsigned int> playerLaps;
	std::vector<Vehicle> playerVehicles;

	pugi::xml_document race_track;
	pugi::xml_document createdTrack;
	pugi::xml_node nodeCheckpoints;

	int64_t timeStart;
	unsigned int record;

	unsigned int _vehicleModel;
	unsigned int _checkpointCount;
	unsigned int trackCount;
	unsigned int laps;

	std::string _vehicleModels;
	std::string _trackName;
	std::string _authorName;
	std::string path;

	float heading;
	float startLateralGap;
	float startBackwardsGap;

	unsigned int playersFinished;

	int64_t endTime;
public:
	RaceSystem() :
		b_raceInProgress(false),
		b_readyToStart(false),
		b_spawnedGrid(false),
		b_loadedRaceTrack(false),
		trackCheckpoints(1000, RaceCheckpointStruct()),
		worldCheckpoints(100, std::pair<Player, SphereCheckpoint>(Player(), SphereCheckpoint())),
		playerCheckpoints(100, -1),
		playerLaps(100, 1),
		playerVehicles(100, Vehicle()),
		race_track(),
		createdTrack(),
		nodeCheckpoints(),
		timeStart(),
		record(3600),
		_vehicleModel(0),
		_checkpointCount(0),
		trackCount(0),
		laps(1),
		_vehicleModels(),
		_trackName("Unknown"),
		_authorName("Unknown"),
		heading(0.0f),
		startLateralGap(0.0f),
		startBackwardsGap(0.0f),
		playersFinished(0),
		endTime(0)
	{
		trackCheckpoints.reserve(100);
	}

	~RaceSystem() {}

	void UpdateTrackList();
	int CreateCarForPlayer(Player player, bool b_atSpawn);
	void MoveToLastCP(Player player);

	void CreateRaceTrack(std::string trackname, std::string author, int32_t model, float heading, float lateralgap, float backwardsgap);
	void CreateCheckpoint(Vector3 checkpoint, float headingAngle);
	void SaveCreatedTrack(std::string path);

	void LoadRaceTrack(std::string file_path);
	void CreateRightRaceGrid(int32_t world, float heading, Vector3 startPosition, unsigned int numberOfVehicles, float leftGap, float backwardsGap);
	void PutPlayersInVehicles();
	void PrepareCheckpoints(Player player);
	void StartRaceCountdown();
	void StartRace();
	void ProcessCheckpoint(Player player);
	void EndRace();
	void ProcessFinish(Player player);
	void ProcessLap(Player player);

	void Debug();
	void Clean(unsigned int nPlayerId);

	bool ReadyToStart();
	bool HasGridBeenSpawned();
	bool HasStarted();

	int32_t worldWeather;
	unsigned int worldHour;
	unsigned int worldMinute;
	unsigned int GetVehicleModel();
	unsigned int GetCheckpointCount();
	unsigned int GetTrackCount();
	std::string GetAuthor();
	std::string GetTrackName();
	float GetHeading();

	Vector3 GetCheckpoint(unsigned int checkpoint);

	int GetPlayerCheckpointProgress(unsigned int nPlayerID);
	std::pair<Player, SphereCheckpoint> GetPlayerCheckpointData(unsigned int playerID);

	void UpdateRecord(std::string record);

	int64_t GetEndTime();
	unsigned int GetEligiblePlayerCount();
};
