#include "Server.h"
#include "ConsoleMessages.h"
#include <cstdio>
#include <cstdarg>

extern PluginFuncs* gFuncs;

vcmpError Server::SetServerName(std::string text)
{
	vcmpError error = gFuncs->SetServerName(text.c_str());
	if (error)
		OutputError("Error in Server::SetServerName. %s", GetErrorMessage(error).c_str());

	return error;
}

vcmpError Server::GetServerName(char * buffer, size_t size)
{
	vcmpError error = gFuncs->GetServerName(buffer, size);
	if (error)
		OutputError("Error in Server::GetServerName. %s", GetErrorMessage(error).c_str());

	return error;
}

vcmpError Server::SetMaxPlayers(uint32_t maxPlayers)
{
	vcmpError error = gFuncs->SetMaxPlayers(maxPlayers);
	if (error)
		OutputError("Error in Server::SetMaxPlayers. %s", GetErrorMessage(error).c_str());

	return error;
}

uint32_t Server::GetMaxPlayers()
{
	return gFuncs->GetMaxPlayers();
}

vcmpError Server::SetServerPassword(std::string password)
{
	vcmpError error = gFuncs->SetServerPassword(password.c_str());
	if (error)
		OutputError("Error in Server::SetServerPassword. %s", GetErrorMessage(error).c_str());

	return error;
}

vcmpError Server::GetServerPassword(char * buffer, size_t size)
{
	return vcmpError();
}

vcmpError Server::SetGameModeText(std::string gameMode)
{
	vcmpError error = gFuncs->SetGameModeText(gameMode.c_str());
	if (error)
		OutputError("Error in Server::SetGameModeText. %s", GetErrorMessage(error).c_str());

	return error;
}

vcmpError Server::GetGameModeText(char * buffer, size_t size)
{
	vcmpError error = gFuncs->GetGameModeText(buffer, size);
	if (error)
		OutputError("Error in Server::GetGameModeText. %s", GetErrorMessage(error).c_str());

	return error;
}

void Server::ShutdownServer()
{
	gFuncs->ShutdownServer();
}

vcmpError Server::SetServerOption(vcmpServerOption option, uint8_t toggle)
{
	vcmpError error = gFuncs->SetServerOption(option, toggle);
	if (error)
		OutputError("Error in Server::SetServerOption. %s", GetErrorMessage(error).c_str());

	return error;
}

uint8_t Server::GetServerOption(vcmpServerOption option)
{
	return uint8_t();
}

Server::~Server()
{
}

uint32_t Server::GetServerVersion()
{
	return gFuncs->GetServerVersion();
}

vcmpError Server::GetServerSettings(ServerSettings * settings)
{
	vcmpError error = gFuncs->GetServerSettings(settings);
	if (error)
		OutputError("Error in Server::GetServerSettings. %s", GetErrorMessage(error).c_str());

	return error;
}

vcmpError Server::ExportFunctions(int32_t pluginId, const void ** functionList, size_t size)
{
	vcmpError error = gFuncs->ExportFunctions(pluginId, functionList, size);
	if (error)
		OutputError("Error in Server::ExportFunctions. %s", GetErrorMessage(error).c_str());

	return error;
}

uint32_t Server::GetNumberOfPlugins()
{
	return gFuncs->GetNumberOfPlugins();
}

vcmpError Server::GetPluginInfo(int32_t pluginId, PluginInfo * pluginInfo)
{
	vcmpError error = gFuncs->GetPluginInfo(pluginId, pluginInfo);
	if (error)
		OutputError("Error in Server::GetPluginInfo. %s", GetErrorMessage(error).c_str());

	return error;
}

int32_t Server::FindPlugin(std::string pluginName)
{
	return gFuncs->FindPlugin(pluginName.c_str());
}

const void ** Server::GetPluginExports(int32_t pluginId, size_t * exportCount)
{
	return nullptr;
}

vcmpError Server::SendPluginCommand(uint32_t commandIdentifier, const char * format, ...)
{
	return vcmpError();
}

uint64_t Server::GetTime()
{
	return gFuncs->GetTime();
}

vcmpError Server::LogMessage(const char * format, ...)
{
	return vcmpError();
}

vcmpError Server::GetLastError()
{
	return gFuncs->GetLastError();
}

int32_t Server::GetKeyBindUnusedSlot()
{
	int32_t value = gFuncs->GetKeyBindUnusedSlot();
	if (value == -1)
		OutputError("Error in Server::GetKeyBindUnusedSlot. Entity does not exist.");

	return value;
}

vcmpError Server::GetKeyBindData(int32_t bindId, uint8_t * isCalledOnReleaseOut, int32_t * keyOneOut, int32_t * keyTwoOut, int32_t * keyThreeOut)
{
	vcmpError error = gFuncs->GetKeyBindData(bindId, isCalledOnReleaseOut, keyOneOut, keyTwoOut, keyThreeOut);
	if (error)
		OutputError("Error in Server::GetKeyBindData. %s", GetErrorMessage(error).c_str());

	return error;
}

vcmpError Server::RegisterKeyBind(int32_t bindId, uint8_t isCalledOnRelease, int32_t keyOne, int32_t keyTwo, int32_t keyThree)
{
	vcmpError error = gFuncs->RegisterKeyBind(bindId, isCalledOnRelease, keyOne, keyTwo, keyThree);
	if (error)
		OutputError("Error in Server::RegisterKeyBind. %s", GetErrorMessage(error).c_str());

	return error;
}

vcmpError Server::RemoveKeyBind(int32_t bindId)
{
	vcmpError error = gFuncs->RemoveKeyBind(bindId);
	if (error)
		OutputError("Error in Server::RemoveKeyBind. %s", GetErrorMessage(error).c_str());

	return error;
}

void Server::RemoveAllKeyBinds()
{
	gFuncs->RemoveAllKeyBinds();
}

vcmpError Server::SendMessage(uint32_t colour, const char * format, ...)
{
	char buf[256];
	va_list begin;

	va_start(begin, format);
	vsnprintf(buf, 256, format, begin);
	va_end(begin);

	
	vcmpError error;
	for (int i = 0; i < 100; ++i)
		if (gFuncs->IsPlayerConnected(i))
		{
			error = gFuncs->SendClientMessage(i, colour, buf);
		
			if (error)
			{
				OutputError("Error in Player::SendGameMessage [%s]", GetErrorMessage(error).c_str());
				break;
			}
		}

	return error;
}
