#pragma once
#include "plugin.h"
#include <string>

class Server
{
private:

public:
	Server()
	{};
	~Server();

	uint32_t GetServerVersion();
	vcmpError GetServerSettings(ServerSettings* settings);
	vcmpError ExportFunctions(int32_t pluginId, const void** functionList, size_t size);
	uint32_t GetNumberOfPlugins();
	vcmpError GetPluginInfo(int32_t pluginId, PluginInfo* pluginInfo);
	int32_t FindPlugin(std::string pluginName);
	const void** GetPluginExports(int32_t pluginId, size_t* exportCount);
	vcmpError SendPluginCommand(uint32_t commandIdentifier, const char* format, ...);
	uint64_t GetTime();
	vcmpError LogMessage(const char* format, ...);
	vcmpError GetLastError();

	vcmpError SetServerName (std::string text);
	vcmpError GetServerName (char* buffer, size_t size);
	vcmpError SetMaxPlayers (uint32_t maxPlayers);
	uint32_t GetMaxPlayers ();
	vcmpError SetServerPassword (std::string password);
	vcmpError GetServerPassword (char* buffer, size_t size);
	vcmpError SetGameModeText (std::string gameMode);
	vcmpError GetGameModeText (char* buffer, size_t size);
	void ShutdownServer ();

	vcmpError SetServerOption (vcmpServerOption option, uint8_t toggle);
	uint8_t GetServerOption (vcmpServerOption option);

	int32_t GetKeyBindUnusedSlot();
	vcmpError GetKeyBindData(int32_t bindId, uint8_t* isCalledOnReleaseOut, int32_t* keyOneOut, int32_t* keyTwoOut, int32_t* keyThreeOut);
	vcmpError RegisterKeyBind(int32_t bindId, uint8_t isCalledOnRelease, int32_t keyOne, int32_t keyTwo, int32_t keyThree);
	vcmpError RemoveKeyBind(int32_t bindId);
	void RemoveAllKeyBinds();

	vcmpError SendMessage(uint32_t colour, const char* format, ...);
};