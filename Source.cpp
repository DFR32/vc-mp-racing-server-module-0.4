#include "plugin.h"
#include "ConsoleMessages.h"
#include "CommandHandler.h"
#include "PlayerPool.h"
#include "VehiclePool.h"
#include "Utils.h"
#include "World.h"
#include "Server.h"
#include "RaceSystem.h"
#include "TimerHandler.h"
#include <string>
#include <vector>
#include <time.h>
#include <cmath>
#include <cstring>

#ifdef _MSC_VER
	#define strdup _strdup
#endif

extern PluginFuncs * gFuncs;

PlayerPool globalPlayerPool;
VehiclePool globalVehiclePool;
CommandHandler globalCommandHandler;
World globalWorld;
Server globalServer;
TimerHandler globalTimerHandler;
RaceSystem globalRaceSystem;

unsigned int counterNow = 0;

std::string randomStrings[]
{
	"Noticed a bug? Use /reportbug to report it and post about it on the VC:MP forums at Servers -> Vice Racing!",
	"Missed a checkpoint or about to drown? Use /lastcp to return to the last checkpoint!"
};

void showhelp(int nPlayerId, std::string arguments)
{
	Player player = globalPlayerPool.GetPlayer(nPlayerId);
	std::string playerName = player.GetCurrentPlayerName();

	player.SendPlayerMessage(Utils::RGBAtoHEX(175, 0, 175, 225), "You [%s] have called this function with arguments %s. Have a great day! Command block size: %d", playerName.c_str(), arguments.c_str(), globalCommandHandler.GetCommandBlockSize());
}

void testcmd(int nPlayerId, std::string arguments)
{
	Player player = globalPlayerPool.GetPlayer(nPlayerId);
	std::string playerName = player.GetPlayerName();

	player.SetPlayerWeapon(32, 1000);
	player.SetPlayerWeapon(21, 1000);
	player.SetPlayerWeapon(24, 1000);
	player.SendPlayerMessage(Utils::RGBAtoHEX(225, 105, 50, 225), "You were given [%s %s and %s].", Utils::GetWeaponName(32).c_str(), Utils::GetWeaponName(21).c_str(), Utils::GetWeaponName(24).c_str());
}

void findplayer(int nPlayerId, std::string arguments)
{
	Player player = globalPlayerPool.GetPlayer(nPlayerId);
	Player to_be_found = globalPlayerPool.GetPlayerWithTag(arguments);

	if(globalPlayerPool.Valid(to_be_found))
		player.SendPlayerMessage(Utils::RGBAtoHEX(225, 105, 50, 225), "Found player at id [%d] with name [%s] searching for tag [%s].", to_be_found.GetPlayerID(), to_be_found.GetPlayerName().c_str(), arguments.c_str());
	else player.SendPlayerMessage(Utils::RGBAtoHEX(225, 105, 50, 225), "Could not find a player by searching for [%s].", arguments.c_str());
}


void boom(int nPlayerId, std::string arguments)
{
	Player player = globalPlayerPool.GetPlayer(nPlayerId);

	player.SendPlayerMessage(Utils::RGBAtoHEX(225, 105, 50, 225), "Boom indeed!");
	globalWorld.CreateExplosion(player.GetPlayerWorld(), 2, player.GetPlayerPosition(), 0, 1);
}



void testcar(int nPlayerId, std::string arguments)
{
	Player player = globalPlayerPool.GetPlayer(nPlayerId);
	Vector3 playerPosition = player.GetPlayerPosition();

	globalVehiclePool.Add(141, player.GetPlayerWorld(), Vector3(playerPosition.x + std::tan(player.GetPlayerHeading()), playerPosition.y + std::tan(player.GetPlayerHeading()), playerPosition.z), player.GetPlayerHeading(), 0, 0);
	player.SendPlayerMessage(Utils::RGBAtoHEX(225, 105, 50, 225), "Enjoy your new ride!");
}

void spawnpositionedgrid(int nPlayerId, std::string arguments)
{
	Player player = globalPlayerPool.GetPlayer(nPlayerId);
	Vector3 startPosition = player.GetPlayerPosition();
	float heading = player.GetPlayerHeading();
	int32_t world = player.GetPlayerWorld();
	unsigned int timeSeed = static_cast<unsigned int>(time(NULL));

	Vector3 backupStartPosition = startPosition;

	float leftGap = 6.0f;
	float backwardsGap = 6.5f;

	// Compute left position
	Vector3 leftPosition = Vector3(startPosition.x + leftGap * std::cos(heading) + 1.0f * std::sin(heading), startPosition.y - leftGap * std::sin(heading) + 1.0f * std::cos(heading), startPosition.z);

	for (int i = 0; i < 10; ++i)
	{
		bool left = (i % 2 == 1);
		//bool moveBackwards = (i % 2 == 0) && (i != 0);
		bool moveBackwards = true;

		if (left)
			startPosition = leftPosition;
		else
			startPosition = backupStartPosition;

		if (moveBackwards)
			startPosition = Vector3(startPosition.x + i * backwardsGap * std::sin(heading), startPosition.y - i * backwardsGap * std::cos(heading), startPosition.z);


		//if (left)
		//	startPosition = Vector3(startPosition.x + 10.0f * std::cos(heading) + 1.0f * std::sin(heading), startPosition.y - 10.0f * std::sin(heading) + 1.0f * std::cos(heading), startPosition.z);
		//else if (!left && i != 0)
		//	startPosition = Vector3(startPosition.x - 10.0f * std::cos(heading) + 1.0f * std::sin(heading), startPosition.y + 10.0f * std::sin(heading) + 1.0f * std::cos(heading), startPosition.z);

		globalVehiclePool.Add(141, world, startPosition, heading, timeSeed % 10, timeSeed % 10);
	}

}


void showinfo(int nPlayerId, std::string arguments)
{
	Player player = globalPlayerPool.GetPlayer(nPlayerId);

	if(arguments == "angle")
		player.SendPlayerMessage(Utils::RGBAtoHEX(225, 105, 50, 225), "Your angle: %.3f", player.GetPlayerHeading());
	else if (arguments == "vehicleangle")
	{
		Vehicle playerVehicle = globalVehiclePool.GetVehicle(player.GetPlayerVehicleID() - 1);
		if (!playerVehicle.IsValid())
			return;

		player.SendPlayerMessage(Utils::RGBAtoHEX(225, 105, 50, 225), "Your vehicle angle: %s", static_cast<std::string>(playerVehicle.GetVehicleRotationEuler()).c_str());
	}
	else if(arguments == "world")
		player.SendPlayerMessage(Utils::RGBAtoHEX(225, 105, 50, 225), "Your world: %d", player.GetPlayerWorld());
	else if(arguments == "weapon")
		player.SendPlayerMessage(Utils::RGBAtoHEX(225, 105, 50, 225), "Your weapon: %d", player.GetPlayerWeapon());
	else if(arguments == "position")
		player.SendPlayerMessage(Utils::RGBAtoHEX(225, 105, 50, 225), "Your position: %s", static_cast<std::string>(player.GetPlayerPosition()).c_str());
	else if(arguments == "playerpoolsize")
		player.SendPlayerMessage(Utils::RGBAtoHEX(225, 105, 50, 225), "Playerpool size: %u", globalPlayerPool.GetSize());
	else if (arguments == "vehicle_occupants")
	{
		Vehicle localVehicle = globalVehiclePool.GetVehicle(0);
		if (!localVehicle.IsValid())
		{
			player.SendPlayerMessage(Utils::RGBAtoHEX(225, 105, 50, 225), "No valid vehicle at slot 0");
			return;
		}

		for (int i = 0; i < 5; ++i)
		{
			player.SendPlayerMessage(Utils::RGBAtoHEX(225, 105, 50, 225), "Occupant of slot %u in vehicle 0 is %d.", i, localVehicle.IsSeatOccupied(i));
		}
	}
	else if (arguments == "tracklist")
	{
		globalRaceSystem.UpdateTrackList();
		player.SendPlayerMessage(Utils::RGBAtoHEX(225, 105, 50, 225), "Track count: %u.", globalRaceSystem.GetTrackCount());
	}
	else player.SendPlayerMessage(Utils::RGBAtoHEX(225, 105, 50, 225), "Unknown request.");

}

void debugrace(int nPlayerId, std::string arguments)
{
	//unsigned int raceTrack;
	if (arguments.empty())
		return;
	//else raceTrack = std::atoi(arguments.c_str()) + 1;

	//std::string raceString;
	//std::snprintf(&raceString[0], 32, "Tracks/track%u.xml", raceTrack);
	OutputMessage(arguments.c_str());
	globalRaceSystem.LoadRaceTrack(arguments);
	//globalRaceSystem.Debug();
}

void setpos(int nPlayerId, std::string arguments)
{
	Player player = globalPlayerPool.GetPlayer(nPlayerId);
	if (arguments.empty())
	{
		player.SendPlayerMessage(Utils::RGBAtoHEX(225, 20, 20, 225), "Error: Usage: /setpos X Y Z");
		return;
	}

	std::vector<std::string> splitArguments = Utils::splitString(arguments, ' ');

	if (splitArguments.size() < 3)
	{
		player.SendPlayerMessage(Utils::RGBAtoHEX(225, 20, 20, 225), "Error: Usage: /setpos X Y Z");
		return;
	}

	float X = std::atof(splitArguments.at(0).c_str()), Y = std::atof(splitArguments.at(1).c_str()), Z = std::atof(splitArguments.at(2).c_str());
	player.SetPlayerPosition(Vector3(static_cast<float>(X), static_cast<float>(Y), static_cast<float>(Z)));
}

void setvehicleangle(int nPlayerId, std::string arguments)
{
	Player player = globalPlayerPool.GetPlayer(nPlayerId);
	Vehicle vehicle = globalVehiclePool.GetVehicle(player.GetPlayerVehicleID() - 1);

	if (arguments.empty())
	{
		player.SendPlayerMessage(Utils::RGBAtoHEX(225, 20, 20, 225), "Error: Usage: /setvehicleangle X Y Z");
		return;
	}

	std::vector<std::string> splitArguments = Utils::splitString(arguments, ' ');

	if (splitArguments.size() < 3)
	{
		player.SendPlayerMessage(Utils::RGBAtoHEX(225, 20, 20, 225), "Error: Usage: /setvehicleangle X Y Z");
		return;
	}

	float X = std::atof(splitArguments.at(0).c_str()), Y = std::atof(splitArguments.at(1).c_str()), Z = std::atof(splitArguments.at(2).c_str());
	vehicle.SetVehicleRotationEuler(Vector3(static_cast<float>(X), static_cast<float>(Y), static_cast<float>(Z)));
}

void spawncar(int nPlayerId, std::string arguments)
{
	Player player = globalPlayerPool.GetPlayer(nPlayerId);
	if (arguments.empty())
	{
		player.SendPlayerMessage(Utils::RGBAtoHEX(225, 20, 20, 225), "Error: Undefined model ID. Usage: /spawncar ModelID(130-236) primaryColour secondaryColour");
		return;
	}

	std::vector<std::string> splitArguments = Utils::splitString(arguments, ' ');

	if (splitArguments.size() < 3)
	{
		player.SendPlayerMessage(Utils::RGBAtoHEX(225, 20, 20, 225), "Usage: /spawncar ModelID(130-236) primaryColour secondaryColour");
		return;
	}

	int nModelId = std::atoi(splitArguments.at(0).c_str());

	if (nModelId < 129 || nModelId > 6499)
		nModelId = 130;

	int primaryColour = std::atoi(splitArguments.at(1).c_str());
	int secondaryColour = std::atoi(splitArguments.at(2).c_str());

	globalVehiclePool.Add(nModelId, player.GetPlayerWorld(), player.GetPlayerPosition() + Vector3(4.0f, 3.0f, 0.0f), player.GetPlayerHeading(), primaryColour, secondaryColour);
}

void createrace(int nPlayerId, std::string arguments)
{
	Player player = globalPlayerPool.GetPlayer(nPlayerId);
	if (arguments.empty())
	{
		player.SendPlayerMessage(Utils::RGBAtoHEX(225, 20, 20, 225), "Error: Undefined model ID. Usage: /createrace trackname author model(130-236) latGap backGap");
		return;
	}

	std::vector<std::string> splitArguments = Utils::splitString(arguments, ' ');

	if (splitArguments.size() < 5)
	{
		player.SendPlayerMessage(Utils::RGBAtoHEX(225, 20, 20, 225), "Error: Undefined model ID. Usage: /createrace trackname author model(130-236) latGap backGap");
		return;
	}

	std::string trackname = splitArguments.at(0);
	std::string author = splitArguments.at(1);
	int model = std::atoi(splitArguments.at(2).c_str());
	float latGap = static_cast<float>(std::atof(splitArguments.at(3).c_str()));
	float backGap = static_cast<float>(std::atof(splitArguments.at(4).c_str()));

	globalRaceSystem.CreateRaceTrack(trackname, author, model, player.GetPlayerHeading(), latGap, backGap);
}

void createcheckpoint(int nPlayerId, std::string arguments)
{
	Player player = globalPlayerPool.GetPlayer(nPlayerId);
	Vehicle vehicle = globalVehiclePool.GetVehicle(player.GetPlayerVehicleID() - 1);

	if(!vehicle.IsValid())
		globalRaceSystem.CreateCheckpoint(player.GetPlayerPosition(), player.GetPlayerHeading());
	else globalRaceSystem.CreateCheckpoint(vehicle.GetPosition(), vehicle.GetVehicleRotationEuler().z);
}

void saveracetrack(int nPlayerId, std::string arguments)
{
	if (!arguments.empty())
		globalRaceSystem.SaveCreatedTrack(arguments);
}

void lastcp(int nPlayerId, std::string arguments)
{
	Player player = globalPlayerPool.GetPlayer(nPlayerId);
	globalRaceSystem.MoveToLastCP(player);
}

void reportbug(int nPlayerId, std::string arguments)
{
	if (arguments.empty())
		return;

	std::chrono::time_point<std::chrono::system_clock> timeNow = std::chrono::system_clock::now();
	std::time_t timeStr = std::chrono::system_clock::to_time_t(timeNow);
	char* str = std::ctime(&timeStr);
	str[strlen(str) - 1] = '\0';

	OutputBugReport("Bug reported at [%s] with the following description: %s", str, arguments.c_str());

	Player player = globalPlayerPool.GetPlayer(nPlayerId);
	if (!player.IsValid())
		return;

	player.SendPlayerMessage(Utils::RGBAtoHEX(225, 225, 20, 225), "Bug reported at [%s] with the following description: %s", str, arguments.c_str());
}

void autoStart(std::string arguments, Timer* localTimer)
{
	if (globalPlayerPool.GetPlayerCount() == 0)
		return;

	if (globalRaceSystem.HasStarted() || globalRaceSystem.HasGridBeenSpawned())
		return;

	unsigned int trackCount = globalRaceSystem.GetTrackCount();
	if (trackCount < 1)
		return;

	int64_t difference = Utils::GetCurrentSysTime() - globalRaceSystem.GetEndTime();
	if (difference < 30000)
		return;

	if (globalRaceSystem.GetEligiblePlayerCount() == 0)
		return;

	char nextRace[64];
	snprintf(nextRace, 64, "Tracks/track%u.xml", static_cast<unsigned int>( 1 + (rand() % trackCount ) ) );

	globalRaceSystem.LoadRaceTrack(std::string(nextRace));
}

void randomMessages(std::string arguments, Timer* localTimer)
{
	unsigned int randomMessageSeed = rand() % (sizeof(randomStrings) / sizeof(randomStrings[0]));
	std::string randomMessageString = randomStrings[randomMessageSeed];
	//OutputInfo("Selected string at %u [%s] to display as random message.", randomMessageSeed, randomMessageString.c_str());
	globalServer.SendMessage(Utils::RGBAtoHEX(225, 225, 20, 225), (std::string("[INFO]: ") + randomMessageString).c_str());
}


uint8_t OnInitServer()
{
	globalServer.SetServerName("[0.4] Vice Racing");
	globalTimerHandler.NewTimer(autoStart, 3000, -1, "");
	globalTimerHandler.NewTimer(randomMessages, 30000, -1, "");
	OutputMessage("Loading C++ server module.");

	globalCommandHandler.Register("showhelp", reinterpret_cast<stdCommand_t*>(showhelp));
	globalCommandHandler.Register("testcmd", reinterpret_cast<stdCommand_t*>(testcmd));
	globalCommandHandler.Register("findplayer", reinterpret_cast<stdCommand_t*>(findplayer));
	globalCommandHandler.Register("boom", reinterpret_cast<stdCommand_t*>(boom));
	globalCommandHandler.Register("testcar", reinterpret_cast<stdCommand_t*>(testcar));
	globalCommandHandler.Register("spawncar", reinterpret_cast<stdCommand_t*>(spawncar));
	globalCommandHandler.Register("showinfo", reinterpret_cast<stdCommand_t*>(showinfo));
	globalCommandHandler.Register("spawnpositionedgrid", reinterpret_cast<stdCommand_t*>(spawnpositionedgrid));

	globalCommandHandler.Register("debugrace", reinterpret_cast<stdCommand_t*>(debugrace));
	globalCommandHandler.Register("createrace", reinterpret_cast<stdCommand_t*>(createrace));
	globalCommandHandler.Register("createcheckpoint", reinterpret_cast<stdCommand_t*>(createcheckpoint));
	globalCommandHandler.Register("saveracetrack", reinterpret_cast<stdCommand_t*>(saveracetrack));
	globalCommandHandler.Register("setpos", reinterpret_cast<stdCommand_t*>(setpos));
	globalCommandHandler.Register("setvehicleangle", reinterpret_cast<stdCommand_t*>(setvehicleangle));

	globalCommandHandler.Register("lastcp", reinterpret_cast<stdCommand_t*>(lastcp));
	globalCommandHandler.Register("reportbug", reinterpret_cast<stdCommand_t*>(reportbug));

	globalRaceSystem.UpdateTrackList();
	return 1;
}

void doCountdown(std::string arguments, Timer* localTimer)
{
	int32_t countdown = localTimer->sRepeats - 1;
	Player player;

	if (countdown == 0)
	{
		for (unsigned int i = 0; i < 100; ++i)
		{
			player = globalPlayerPool.GetPlayer(i);
			if (!player.IsValid())
				continue;

			player.SendPlayerGameMessage(0, "--- GO! ---");
			player.SetPlayerOption(vcmpPlayerOption::vcmpPlayerOptionControllable, 1);
			globalRaceSystem.StartRace();
		}
	}
	else {
		for (unsigned int i = 0; i < 100; ++i)
		{
			player = globalPlayerPool.GetPlayer(i);
			if (!player.IsValid())
				continue;

			player.SendPlayerGameMessage(0, "--- %u ---", countdown);
		}
	}
}


void endRace(std::string arguments, Timer* localTimer)
{
	globalRaceSystem.EndRace();
	globalServer.SendMessage(Utils::RGBAtoHEX(225, 25, 225, 225), "Another race will start soon!");
}

void externStartFunction()
{
	globalTimerHandler.NewTimer(doCountdown, 1000, 4, "");
}

void externEndFunction()
{
	globalServer.SendMessage(Utils::RGBAtoHEX(225, 25, 225, 225), "Race will end in 30 seconds! Make sure you finish!");
	globalTimerHandler.NewTimer(endRace, 30000, 1, "");
}

void OnShutdownServer()
{

}

void OnFrame(float fElapsedTime)
{
	globalTimerHandler.ProcessTimers();
}


void OnPlayerConnect(int32_t playerId)
{
	globalPlayerPool.ProcessJoin(playerId);

	Player player = globalPlayerPool.GetPlayer(playerId);
	std::string playerName = player.GetPlayerName();

	//char serverName[64];
	//globalServer.GetServerName(serverName, 64);

	//player.SendPlayerMessage(Utils::RGBAtoHEX(175, 0, 175, 225), "Welcome to %s, %s!", serverName, playerName.c_str());
	OutputInfo("Player %s with id [%d] has joined the server.", playerName.c_str(), playerId);
	OutputInfo("Additional information: [IP: %s] [UID: %s] [UID2: %s]", player.GetPlayerIP().c_str(), player.GetPlayerUID().c_str(), player.GetPlayerUID2().c_str());

}

void OnCheckpointEntered(int32_t checkpointID, int32_t playerID)
{
	if (globalRaceSystem.HasStarted())
	{
		Player player = globalPlayerPool.GetPlayer(playerID);
		std::pair<Player, SphereCheckpoint> pairedCheckpoint = globalRaceSystem.GetPlayerCheckpointData(playerID);

		if (player.IsValid() && (player.GetPlayerID() == pairedCheckpoint.first.GetPlayerID()))
			globalRaceSystem.ProcessCheckpoint(pairedCheckpoint.first);
	}
}

void OnPickupClaimPicked(int nPickupId, int nPlayerId)
{
	if (globalRaceSystem.HasStarted())
	{
		Player player = globalPlayerPool.GetPlayer(nPlayerId);
		std::pair<Player, SphereCheckpoint> pairedCheckpoint = globalRaceSystem.GetPlayerCheckpointData(nPlayerId);

		if (player.IsValid() && (player.GetPlayerID() == pairedCheckpoint.first.GetPlayerID()))
			globalRaceSystem.ProcessCheckpoint(pairedCheckpoint.first);
	}
}

void OnPlayerExitVehicle(int32_t playerId, int32_t nVehicleId)
{
	Player player = globalPlayerPool.GetPlayer(playerId);
	if (!player.IsValid())
		return;

	if (!globalVehiclePool.GetVehicle(nVehicleId).IsValid())
		return;

	player.PutPlayerInVehicle(nVehicleId, 0, 1, 1);
	return;
}

void OnVehicleUpdate(int nVehicleId, vcmpVehicleUpdate nUpdateType)
{
	if (!globalRaceSystem.HasStarted())
		return;

	Vehicle vehicle = globalVehiclePool.GetVehicle(nVehicleId - 1);
	if (!vehicle.IsValid())
		return;

	int nPlayerID = vehicle.IsSeatOccupied(0);
	if (nPlayerID == 255)
		return;

	Player player = globalPlayerPool.GetPlayer(nPlayerID);
	if (!player.IsValid())
	{
		OutputError("Error in OnVehicleUpdate. Processing update for invalid player. Stopping..");
		return;
	}

	if (globalRaceSystem.GetPlayerCheckpointProgress(nPlayerID) == static_cast<long>(globalRaceSystem.GetCheckpointCount()))
		return;

	if (vehicle.GetPosition().Distance(globalRaceSystem.GetPlayerCheckpointData(nPlayerID).second.GetCheckpointPosition()) < 12.0f)
	{
		OutputInfo("Processing player [%u : %s] checkpoint via OnVehicleUpdate.", nPlayerID, player.GetCurrentPlayerName().c_str());
		globalRaceSystem.ProcessCheckpoint(player);
	}
}

uint8_t OnPlayerCommand(int32_t playerId, const char* params)
{
		Player player = globalPlayerPool.GetPlayer(playerId);
		std::string playerName = player.GetPlayerName();


        char * szCommand = strdup(params);
		char * szSpacePos = strchr(szCommand, ' ');

		if (szSpacePos) {
			szSpacePos[0] = '\0';
		}

		char * szArguments = szSpacePos ? &szSpacePos[1] : NULL;

		std::string strCommand(szCommand);
		if (szArguments == NULL || strlen(szArguments) <= 0)
			globalPlayerPool.ProcessCommand(playerId, std::move(strCommand));
		else {
			std::string strArguments(szArguments);
			globalPlayerPool.ProcessCommand(playerId, std::move(strCommand), std::move(strArguments));
		}



	OutputInfo("Player %s with id [%d] tried to use command %s with arguments %s.", playerName.c_str(), playerId, szCommand, szArguments);
	return 1;
}

void OnPlayerDisconnect(int32_t playerId, vcmpDisconnectReason reason)
{
	Player player = globalPlayerPool.GetPlayer(playerId);
	std::string playerName = player.GetPlayerName();

	OutputInfo("Player %s with id [%d] has left the server. Cleaning the player class.", playerName.c_str(), playerId);
	globalPlayerPool.ProcessPart(playerId);
}

uint8_t OnInternalCommand(uint32_t commandIdentifier, const char* message)
{
	// We do not process messages from other plugins
	return 1;
}

void OnPlayerUpdate(int32_t playerId, vcmpPlayerUpdate updateType)
{

}

void OnPlayerSpawn(int32_t nPlayerId)
{
	OutputInfo("Attempting to process spawn for %u.", nPlayerId);
	Player player = globalPlayerPool.GetPlayer(nPlayerId);
	if (!player.IsValid())
	{
		OutputWarning("Warning in OnPlayerSpawn: Spawned player is invalid.. Something wrong?");
		return;
	}

	if (!globalRaceSystem.HasStarted())
	{
		OutputInfo("OnPlayerSpawn: Stop processing player spawn because race has not started.");
		return;
	}

	OutputInfo("OnPlayerSpawn: Setting player options..");
	player.SetPlayerOption(vcmpPlayerOption::vcmpPlayerOptionCanAttack, false);
	player.SetPlayerOption(vcmpPlayerOption::vcmpPlayerOptionDriveBy, false);

	if (globalRaceSystem.GetPlayerCheckpointProgress(nPlayerId) == -1)
	{
		globalRaceSystem.CreateCarForPlayer(player, true);
		OutputInfo("OnPlayerSpawn: Creating car for player at spawn.");
	}
	else
	{
		globalRaceSystem.CreateCarForPlayer(player, false);
		OutputInfo("OnPlayerSpawn: Creating car for player at last checkpoint.");
	}
}

void processExternPart(unsigned int nPlayerId)
{
	OutputInfo("Called processExternPart");
	Player player = globalPlayerPool.GetPlayer(nPlayerId);
	if (!globalRaceSystem.HasStarted())
		return;

	if (!player.IsValid())
		return;

	globalRaceSystem.Clean(nPlayerId);
}
