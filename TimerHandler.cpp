#include "TimerHandler.h"
#include "ConsoleMessages.h"

TimerHandler::~TimerHandler()
{
}

int32_t TimerHandler::FindEmptyTimerPoolSlot()
{
	Timer tmpTimer;
	for (unsigned int i = 0; i < MAX_TIMERS; ++i)
	{
		tmpTimer = this->timerVector.at(i);

		if (tmpTimer.bActive) continue;

		return i;
	}
	return -1;
}

void TimerHandler::ProcessTimers()
{
	unsigned int i = 0;
	int64_t difference;
	int64_t timeNow = Utils::GetCurrentSysTime();

	for(auto & tmpTimer : this->timerVector)
	{
		if (!tmpTimer.bActive)
			continue;

		++i;
		difference = ((timeNow - tmpTimer.timeLastProcessed) / 1000);
		if (difference > tmpTimer.uInterval)
		{
			tmpTimer.Execute();
			tmpTimer.timeLastProcessed = timeNow;
			if (tmpTimer.sRepeats > 0)
			{
				tmpTimer.sRepeats--;

				if (tmpTimer.sRepeats == 0)
					tmpTimer.Destroy();
			}
		}
	}
}


void TimerHandler::DestroyTimer(unsigned int idx)
{
	this->timerVector.at(idx).Destroy();
}

Timer TimerHandler::GetTimer(unsigned int timerID)
{
	return this->timerVector.at(timerID);
}

size_t TimerHandler::GetTimerCount()
{
	return this->timerVector.size();
}
