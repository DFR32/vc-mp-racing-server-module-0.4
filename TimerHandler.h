#pragma once
#include "Timers.h"
#include "Utils.h"
#include "ConsoleMessages.h"
#include <vector>
#include <utility> // std::forward

#define MAX_TIMERS 1024

class TimerHandler
{
private:
	std::vector<Timer> timerVector;

public:
	TimerHandler() :
		timerVector(1024, Timer())
	{
	}

	~TimerHandler();

	template < typename F > void NewTimer(F fn, unsigned int repeatInterval, int32_t repeats, std::string arguments)
	{
        int32_t slot = this->FindEmptyTimerPoolSlot();
        if (slot == -1)
        {
            OutputError("Error in TimerHandler::NewTimer. Cannot create anymore timers. Timer pool is full!");
            return;
        }

        OutputInfo("Initializing timer at slot %u.", slot);
        this->timerVector.at(slot).Initialize(std::forward< F >(fn), arguments, repeatInterval, repeats);
	}
	void ProcessTimers();

	void DestroyTimer(unsigned int idx);

	Timer GetTimer(unsigned int timerID);
	size_t GetTimerCount();

	int32_t FindEmptyTimerPoolSlot();
};
