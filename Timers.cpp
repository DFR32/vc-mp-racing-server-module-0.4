#include "Timers.h"
#include "ConsoleMessages.h"
#include "Utils.h"

void Timer::Destroy()
{
	this->bActive = false;
	this->fnCallback = Callback();
	this->funcArguments.clear();
	this->timeLastProcessed = 0;
	this->uInterval = 0;
	this->sRepeats = 0;

	OutputMessage("Destroyed timer!");
}
