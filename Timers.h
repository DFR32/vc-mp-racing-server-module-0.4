#pragma once
#include <string>
#include <utility> // std::forward
#include <functional>
#include "Utils.h"

class Timer
{
	friend class TimerHandler;

    // schimba in (const std::string, & Timer &) cand ai timp
    typedef std::function< void (std::string, Timer *) > Callback;

	bool bActive;
	Callback fnCallback;
	std::string funcArguments;

	int64_t timeLastProcessed;

public:
	unsigned int uInterval;
	int32_t sRepeats;


	Timer() :
		bActive(false),
		fnCallback(),
		funcArguments(),
		timeLastProcessed(0),
		uInterval(0),
		sRepeats(0)
	{}

	~Timer() {}

	template < typename F >void Initialize(F fn, std::string arguments, unsigned int repeatInts, int32_t repeats)
    {
        this->bActive = true;
        this->fnCallback = Callback(std::forward< F >(fn));
        this->funcArguments = arguments;
        this->uInterval = repeatInts;
        this->sRepeats = repeats;
        this->timeLastProcessed = Utils::GetCurrentSysTime();
	}
	void Execute()
	{
        if (fnCallback)
        {
            fnCallback(this->funcArguments, this);
        }
	}
	void Destroy();
};

typedef void(* Timer_t)(std::string arguments, Timer* pTimer);
