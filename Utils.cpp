#include "Utils.h"
#include <vector>
#include <algorithm>
#include <locale>
#include <fstream>

#ifdef _WIN32
#include <Windows.h>

#if defined(_WIN64) && (_WIN32_WINNT < 0x0600)
// We need this for the GetTickCount64() function
#undef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif // _SQ64

#else
#include <time.h>
#endif


uint32_t Utils::RGBAtoHEX(unsigned int r, unsigned int g, unsigned int b, unsigned int a)
{
	return static_cast<uint32_t>(((r & 0xFF) << 24) | ((g & 0xFF) << 16) | ((b & 0xFF) << 8) | (a & 0xFF));
}

std::string Utils::GetWeaponName(int32_t weapon)
{
	switch (weapon)
	{
	case 0:
		return std::string("Unarmed");
	case 1:
		return std::string("Brass Knuckles");
	case 2:
		return std::string("Screwdriver");
	case 3:
		return std::string("Golf Club");
	case 4:
		return std::string("Nightstick");
	case 5:
		return std::string("Knife");
	case 6:
		return std::string("Baseball Bat");
	case 7:
		return std::string("Hammer");
	case 8:
		return std::string("Meat Cleaver");
	case 9:
		return std::string("Machete");
	case 10:
		return std::string("Katana");
	case 11:
		return std::string("Chainsaw");
	case 12:
		return std::string("Grenade");
	case 13:
		return std::string("Remote Detonation Grenade");
	case 14:
		return std::string("Tear Gas");
	case 15:
		return std::string("Molotov Cocktails");
	case 16:
		return std::string("Rocket");
	case 17:
		return std::string("Colt .45");
	case 18:
		return std::string("Python");
	case 19:
		return std::string("Pump-Action Shotgun");
	case 20:
		return std::string("SPAS-12 Shotgun");
	case 21:
		return std::string("Stubby Shotgun");
	case 22:
		return std::string("TEC-9");
	case 23:
		return std::string("Uzi");
	case 24:
		return std::string("Silenced Ingram");
	case 25:
		return std::string("MP5");
	case 26:
		return std::string("M4");
	case 27:
		return std::string("Ruger");
	case 28:
		return std::string("Sniper Rifle");
	case 29:
		return std::string("Laserscope Sniper Rifle");
	case 30:
		return std::string("Rocket Launcher");
	case 31:
		return std::string("Flamethrower");
	case 32:
		return std::string("M60");
	case 33:
		return std::string("Minigun");
	case 34:
		return std::string("Explosion");
	case 35:
		return std::string("Helicannon");
	case 36:
		return std::string("Camera");
	case 39:
		return std::string("Vehicle");
	case 41:
		return std::string("Explosion");
	case 42:
		return std::string("Driveby");
	case 43:
		return std::string("Drowned");
	case 44:
		return std::string("Fall");
	case 51:
		return std::string("Explosion");
	case 70:
		return std::string("Suicide");
	default:
		return std::string("Unknown");
	}
}

std::string Utils::LowerStr(std::string stringToLower)
{
	std::transform(stringToLower.begin(), stringToLower.end(), stringToLower.begin(), ::tolower);
	return std::move(stringToLower);
}

void Utils::split(const std::string & s, char delim, std::vector<std::string>& elems)
{
	std::stringstream ss(s);
	std::string item;
	while (getline(ss, item, delim)) {
		if(!item.empty())
		elems.push_back(item);
	}
}

std::vector<std::string> Utils::splitString(const std::string & s, char delim)
{
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}

Vector3 Utils::GetLeftPosition(Vector3 position, float angle, float gap)
{
	return Vector3(position.x + gap * std::cos(angle), position.y - gap * std::sin(angle), position.z);;
}

Vector3 Utils::GetFrontPosition(Vector3 position, float angle, float gap)
{
	return Vector3(position.x - gap * std::sin(angle), position.y + gap * std::cos(angle), position.z);
}


Vector3 Utils::GetBackPosition(Vector3 position, float angle, float gap)
{
	return Vector3(position.x + gap * std::sin(angle), position.y - gap * std::cos(angle), position.z);
}

Vector3 Utils::GetRightPosition(Vector3 position, float angle, float gap)
{
	return Vector3(position.x - gap * std::cos(angle), position.y + gap * std::sin(angle), position.z);;
}

std::string Utils::GetPlaceStrFromNumber(unsigned int place)
{
	char buf[8];
	unsigned int lastDigit = place % 10;
	if ((lastDigit < 4) && (lastDigit != 0))
	{
		if (place > 9 && place < 20)
		{
			std::snprintf(buf, 8, "%uth", place);
		}
		else if (lastDigit == 1)
			std::snprintf(buf, 8, "%ust", place);
		else if (lastDigit == 2)
			std::snprintf(buf, 8, "%und", place);
		else if (lastDigit == 3)
			std::snprintf(buf, 8, "%urd", place);
	}
	else std::snprintf(buf, 8, "%uth", place);
	return std::string(buf);
}

bool Utils::IsFileReadable(char* path)
{
	std::ifstream file(path);
	if (file.good())
		return true;

	return false;
}

#if defined(_WIN32) || defined(WIN32)
inline LARGE_INTEGER GetFrequency()
{
	LARGE_INTEGER frequency;
	QueryPerformanceFrequency(&frequency);
	return frequency;
}

int64_t Utils::GetCurrentSysTime()
{
		// Force the following code to run on first core
		// (see http://msdn.microsoft.com/en-us/library/windows/desktop/ms644904(v=vs.85).aspx)
		HANDLE current_thread = GetCurrentThread();
		DWORD_PTR previous_mask = SetThreadAffinityMask(current_thread, 1);

		// Get the frequency of the performance counter
		// (it is constant across the program lifetime)
		static const LARGE_INTEGER frequency = GetFrequency();

		// Get the current time
		LARGE_INTEGER time;
		QueryPerformanceCounter(&time);

		// Restore the thread affinity
		SetThreadAffinityMask(current_thread, previous_mask);

		// Return the current time as microseconds
		return static_cast<int64_t>(1000000LL * time.QuadPart / frequency.QuadPart);
}
#else
int64_t Utils::GetCurrentSysTime()
{
	// POSIX implementation
	timespec time;
	clock_gettime(CLOCK_MONOTONIC, &time);
	return static_cast<int64_t>(uint64_t(time.tv_sec) * 1000000 + time.tv_nsec / 1000);
}
#endif
