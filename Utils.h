#pragma once
#include <stdint.h>
#include <string>
#include <vector>
#include <string>
#include <sstream>
#include "Vector3.h"

namespace Utils
{
	uint32_t RGBAtoHEX(unsigned int r, unsigned int g, unsigned int b, unsigned int a);
	std::string GetWeaponName(int32_t id);
	std::string LowerStr(std::string stringToBeLowered);
	void split(const std::string &s, char delim, std::vector<std::string> &elems);
	std::vector<std::string> splitString(const std::string &s, char delim);
	Vector3 GetLeftPosition(Vector3 position, float angle, float gap);
	Vector3 GetFrontPosition(Vector3 position, float angle, float gap);
	Vector3 GetBackPosition(Vector3 position, float angle, float gap);
	Vector3 GetRightPosition(Vector3 position, float angle, float gap);
	std::string GetPlaceStrFromNumber(unsigned int place);
	bool IsFileReadable(char* path);
	int64_t GetCurrentSysTime();
}
