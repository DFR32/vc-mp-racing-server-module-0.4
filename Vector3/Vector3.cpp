#include "Vector3.h"
#include <cmath>

Vector3::Vector3()
{
		this->b_Initialised = false;
		this->x = 0.0f;
		this->y = 0.0f;
		this->z = 0.0f;
}

Vector3::Vector3(float xCoord, float yCoord, float zCoord)
{
		this->b_Initialised = true;
		this->x = xCoord;
		this->y = yCoord;
		this->z = zCoord;
}

Vector3 Vector3::operator+(const Vector3& vector)
{
		Vector3 return_vector;
		return_vector.x = this->x + vector.x;
		return_vector.y = this->y + vector.y;
		return_vector.z = this->z + vector.z;
		return return_vector;
}

Vector3 Vector3::operator+=(const Vector3& vector)
{
		this->x += vector.x;
		this->y += vector.y;
		this->z += vector.z;
		return *this;
}

Vector3 Vector3::operator-(const Vector3& vector)
{
		Vector3 return_vector;
		return_vector.x = this->x - vector.x;
		return_vector.y = this->y - vector.y;
		return_vector.z = this->z - vector.z;
		return return_vector;
}

Vector3 Vector3::operator-=(const Vector3& vector)
{
		this->x -= vector.x;
		this->y -= vector.y;
		this->z -= vector.z;
		return *this;
}

Vector3 Vector3::operator*(float scalar)
{
		Vector3 return_vector;
		return_vector.x = this->x * scalar;
		return_vector.y = this->y * scalar;
		return_vector.z = this->z * scalar;
		return return_vector;
}

Vector3 Vector3::operator*=(float scalar)
{
		this->x *= scalar;
		this->y *= scalar;
		this->z *= scalar;
		return *this;
}

Vector3 Vector3::Normalize()
{
	float magnitude = this->GetMagnitude();
	return Vector3(this->x / magnitude, this->y / magnitude, this->z / magnitude);
}

float Vector3::GetMagnitude()
{
	return std::sqrt(std::pow(this->x, 2.0f) + std::pow(this->y, 2.0f) + std::pow(this->z, 2.0f));
}

Vector3::operator std::string() const
{
	std::string s(32, '\0');
	const int ret = std::snprintf(&s[0], 32, "X: %.3f Y: %.3f Z: %.3f", this->x, this->y, this->z);
	s.resize(ret < 0 ? 0 : static_cast<unsigned>(ret));
	return std::move(s);
}

float Vector3::Distance(Vector3 target)
{
	return std::sqrt(std::pow((this->x - target.x), 2.0f) + std::pow((this->y - target.y), 2.0f) + std::pow((this->z - target.z), 2.0f));
}

void Vector3::Reset()
{
	this->x = 0.0f;
	this->y = 0.0f;
	this->z = 0.0f;
}

bool Vector3::IsInitialised()
{
	return this->b_Initialised;
}
