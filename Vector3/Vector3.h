#pragma once
#include <string>

class Vector3
{
private:
	bool b_Initialised;

public:
	float x, y, z;


	Vector3();
	Vector3(float xCoord, float yCoord, float zCoord);

	Vector3 operator+(const Vector3& vector);
	Vector3 operator+=(const Vector3& vector);


	Vector3 operator-(const Vector3& vector);
	Vector3 operator-=(const Vector3& vector);


	Vector3 operator*(float scalar);
	Vector3 operator*=(float scalar);

	Vector3 Normalize();
	float GetMagnitude();

	operator std::string() const;

	float Distance(Vector3 target);
	void Reset();
	bool IsInitialised();
};
