#include "Vehicle.h"
#include "ConsoleMessages.h"
#include "Utils.h"
extern PluginFuncs* gFuncs;

Vehicle::~Vehicle()
{
	this->b_Initialized = false;
	this->nVehicleId = 1001;
}

unsigned int Vehicle::GetVehicleID()
{
	if (!this->b_Initialized)
		OutputError("Error in Vehicle::GetVehicleID. Cannot return ID of a non-initialized vehicle.");

	return this->nVehicleId;
}

int32_t Vehicle::Create(int32_t model, int32_t world, Vector3 position, float angle, int32_t pColour, int32_t sColour)
{
	int32_t vehicle = gFuncs->CreateVehicle(model, world, position.x, position.y, position.z, angle, pColour, sColour);
	if (vehicle < 0 || vehicle > 999)
		OutputError("Error in Vehicle::Create. %s", GetErrorMessage(gFuncs->GetLastError()).c_str());

	this->b_Initialized = true;
	this->nVehicleId = vehicle;

	return vehicle;
}

void Vehicle::Destroy()
{
	OutputVerboseInfo("Destroyed vehicle...");
	gFuncs->DeleteVehicle(this->nVehicleId);
	this->b_Initialized = false;
	this->nVehicleId = 1001;
}

Vector3 Vehicle::GetPosition()
{
	if (!this->b_Initialized)
		OutputError("Error in Vehicle::GetPosition. Cannot get postion of inexistent vehicle.");

	float fX, fY, fZ;
	gFuncs->GetVehiclePosition(this->nVehicleId, &fX, &fY, &fZ);
	return Vector3(fX, fY, fZ);
}

Vector3 Vehicle::GetVehicleRotationEuler()
{
	if (!this->b_Initialized)
		OutputError("Error in Vehicle::GetRotationEuler. Cannot get euler rotation of inexistent vehicle.");

	float fX, fY, fZ;
	gFuncs->GetVehicleRotationEuler(this->nVehicleId, &fX, &fY, &fZ);
	return Vector3(fX, fY, fZ);
}

void Vehicle::SetPosition(Vector3 position, uint8_t removeOcupants)
{
	if (!this->b_Initialized)
		OutputError("Error in Vehicle::GetPosition. Cannot set postion of inexistent vehicle.");

	gFuncs->SetVehiclePosition(this->nVehicleId, position.x, position.y, position.z, removeOcupants);
}

void Vehicle::SetVehicleRotationEuler(Vector3 rotation)
{
	if (!this->b_Initialized)
		OutputError("Error in Vehicle::SetRotationEuler. Cannot set euler rotation of inexistent vehicle.");

	gFuncs->SetVehicleRotationEuler(this->nVehicleId, rotation.x, rotation.y, rotation.z);
}

void Vehicle::SetVehicleRotation(float x, float y, float z, float w)
{
	if (!this->b_Initialized)
		OutputError("Error in Vehicle::SetRotation. Cannot set rotation of inexistent vehicle.");

	gFuncs->SetVehicleRotation(this->nVehicleId, x, y, z, w);
}

void Vehicle::GetVehicleRotation(float * xOut, float * yOut, float * zOut, float * wOut)
{
	if (!this->b_Initialized)
		OutputError("Error in Vehicle::GetRotation. Cannot get euler rotation of inexistent vehicle.");

	float fX, fY, fZ, fW;
	gFuncs->GetVehicleRotation(this->nVehicleId, &fX, &fY, &fZ, &fW);
}

bool Vehicle::IsValid()
{
	return this->b_Initialized;
}

int32_t Vehicle::IsSeatOccupied(unsigned int seat)
{
	if (!this->b_Initialized)
		OutputError("Error in Vehicle::IsSeatOccupied. Inexistent vehicle.");

	return gFuncs->GetVehicleOccupant(this->nVehicleId, seat);
	return false;
}

vcmpError Vehicle::SetDoorsLocked(bool toggle)
{
	if (!this->b_Initialized)
		OutputError("Error in Vehicle::SetDoorsLocked. Inexistent vehicle.");

	vcmpError error = gFuncs->SetVehicleOption(this->nVehicleId, vcmpVehicleOption::vcmpVehicleOptionDoorsLocked, toggle ? 1 : 0);
	if (error)
		OutputError("Error in Vehicle::SetDoorsLocked. %s", GetErrorMessage(error).c_str());
	return error;
}

vcmpError Vehicle::SetLights(bool toggle)
{
	if (!this->b_Initialized)
		OutputError("Error in Vehicle::SetLights. Inexistent vehicle.");

	vcmpError error = gFuncs->SetVehicleOption(this->nVehicleId, vcmpVehicleOption::vcmpVehicleOptionLights, toggle ? 1 : 0);
	if (error)
		OutputError("Error in Vehicle::SetLights. %s", GetErrorMessage(error).c_str());
	return error;
}

vcmpError Vehicle::SetImmune(bool toggle)
{
	if (!this->b_Initialized)
		OutputError("Error in Vehicle::SetImmune. Inexistent vehicle.");

	vcmpError error = gFuncs->SetVehicleImmunityFlags(this->nVehicleId, 4294967295);
	if (error)
		OutputError("Error in Vehicle::SetImmune. %s", GetErrorMessage(error).c_str());
	return error;
}
