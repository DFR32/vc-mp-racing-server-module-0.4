#pragma once
#include "plugin.h"
#include "Vector3.h"

class Vehicle
{
	friend class VehiclePool;

private:
	unsigned int nVehicleId;
	bool b_Initialized;

public:
	Vehicle() : nVehicleId(1001), b_Initialized(false) {}
	~Vehicle();

	unsigned int GetVehicleID();

	int32_t Create(int32_t model, int32_t world, Vector3 position, float angle, int32_t pColour, int32_t sColour);
	void Destroy();

	Vector3 GetPosition();
	Vector3 GetVehicleRotationEuler();

	void SetPosition(Vector3 position, uint8_t removeOcupants);
	void SetVehicleRotationEuler(Vector3 rotation);

	void SetVehicleRotation (float x, float y, float z, float w);
	void GetVehicleRotation (float* xOut, float* yOut, float* zOut, float* wOut);

	bool IsValid();
	int32_t IsSeatOccupied(unsigned int seat);

	vcmpError SetDoorsLocked(bool toggle);
	vcmpError SetLights(bool toggle);
	vcmpError SetImmune(bool toggle);
};
