#include "VehiclePool.h"
#include "ConsoleMessages.h"

int32_t VehiclePool::FindSlot()
{
	for (std::vector<Vehicle>::iterator iterator = this->m_vVehiclePool.begin(); iterator != this->m_vVehiclePool.end(); ++iterator)
	{
		if (!(*iterator).b_Initialized) return static_cast<int32_t>(std::distance(this->m_vVehiclePool.begin(), iterator));
	}
	return -1;
}

int32_t VehiclePool::Add(int32_t model, int32_t world, Vector3 position, float angle, int32_t pColour, int32_t sColour)
{
	int32_t freeSlot = this->FindSlot();
	if (freeSlot == -1)
	{
		OutputError("Error in VehiclePool::Add. Vehicle pool is full!");
		return -1;
	}


	int32_t vehicle = this->m_vVehiclePool[freeSlot].Create(model, world, position, angle, pColour, sColour);
	if (vehicle < 0 || vehicle > 999)
	{
		OutputError("Error in VehiclePool::Add. Could not create the vehicle. Invalid arguments.");
		return -1;
	}

	return freeSlot;
}

void VehiclePool::Destroy(unsigned int idx)
{
	if (idx < 0 || idx > 999)
	{
		OutputError("Error in VehiclePool::Destroy. Invalid vehicle id.");
		return;
	}

	OutputVerboseInfo("Destroying vehicle at ID: %u from vehicle pool.", idx);
	this->m_vVehiclePool.at(idx).Destroy();
}

Vehicle VehiclePool::GetVehicle(unsigned int idx)
{
	if(idx > 999)
	{
		OutputError("Error in VehiclePool::GetVehicle. Invalid vehicle id.");
		return this->invalidVehicle;
	}

	return this->m_vVehiclePool.at(idx);
}

bool VehiclePool::Clean(unsigned int idx)
{
	if (idx < 0 || idx > 999)
	{
		OutputError("Error in VehiclePool::Clean. Invalid vehicle id.");
		return false;
	}

	return !this->m_vVehiclePool.at(idx).b_Initialized;
}

bool VehiclePool::Valid(unsigned int idx)
{
	if (idx < 0 || idx > 999)
	{
		OutputError("Error in VehiclePool::Valid. Invalid vehicle id.");
		return false;
	}

	return this->m_vVehiclePool.at(idx).b_Initialized;
}

bool VehiclePool::Valid(Vehicle vehicle)
{
	return vehicle.b_Initialized;
}
