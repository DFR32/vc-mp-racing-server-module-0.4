#pragma once
#include "Vehicle.h"
#include <vector>

class VehiclePool
{
private:
	std::vector<Vehicle> m_vVehiclePool;
	Vehicle invalidVehicle;

public:
	VehiclePool() :
		m_vVehiclePool(1000, Vehicle())
	{}

	~VehiclePool() {}

	int32_t FindSlot();

	int32_t Add(int32_t model, int32_t world, Vector3 position, float angle, int32_t pColour, int32_t sColour);

	void Destroy(unsigned int idx);

	Vehicle GetVehicle(unsigned int idx);

	bool Clean(unsigned int idx);
	bool Valid(unsigned int idx);
	bool Valid(Vehicle vehicle);
};