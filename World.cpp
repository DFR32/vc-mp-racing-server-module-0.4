#include "World.h"
#include "ConsoleMessages.h"

extern PluginFuncs* gFuncs;

World::~World()
{
}

void World::SetWorldBounds(float maxX, float minX, float maxY, float minY)
{
	gFuncs->SetWorldBounds(maxX, minX, maxY, minY);
}

void World::GetWorldBounds(float * maxXOut, float * minXOut, float * maxYOut, float * minYOut)
{
	gFuncs->GetWorldBounds(maxXOut, minXOut, maxYOut, minYOut);
}

void World::SetWastedSettings(uint32_t deathTimer, uint32_t fadeTimer, float fadeInSpeed, float fadeOutSpeed, uint32_t fadeColour, uint32_t corpseFadeStart, uint32_t corpseFadeTime)
{
	gFuncs->SetWastedSettings(deathTimer, fadeTimer, fadeInSpeed, fadeOutSpeed, fadeColour, corpseFadeStart, corpseFadeTime);
}

void World::GetWastedSettings(uint32_t * deathTimerOut, uint32_t * fadeTimerOut, float * fadeInSpeedOut, float * fadeOutSpeedOut, uint32_t * fadeColourOut, uint32_t * corpseFadeStartOut, uint32_t * corpseFadeTimeOut)
{
	gFuncs->GetWastedSettings(deathTimerOut, fadeTimerOut, fadeInSpeedOut, fadeOutSpeedOut, fadeColourOut, corpseFadeStartOut, corpseFadeTimeOut);
}

void World::SetTimeRate(int32_t timeRate)
{
	gFuncs->SetTimeRate(timeRate);
}

int32_t World::GetTimeRate()
{
	return gFuncs->GetTimeRate();
}

void World::SetHour(int32_t hour)
{
	gFuncs->SetHour(hour);
}

int32_t World::GetHour()
{
	return gFuncs->GetHour();
}

void World::SetMinute(int32_t minute)
{
	gFuncs->SetMinute(minute);
}

int32_t World::GetMinute()
{
	return gFuncs->GetMinute();
}

void World::SetWeather(int32_t weather)
{
	gFuncs->SetWeather(weather);
}

int32_t World::GetWeather()
{
	return gFuncs->GetWeather();
}

void World::SetGravity(float gravity)
{
	gFuncs->SetGravity(gravity);
}

float World::GetGravity()
{
	return gFuncs->GetGravity();
}

void World::SetGameSpeed(float gameSpeed)
{
	gFuncs->SetGameSpeed(gameSpeed);
}

float World::GetGameSpeed()
{
	return gFuncs->GetGameSpeed();
}

void World::SetWaterLevel(float waterLevel)
{
	gFuncs->SetWaterLevel(waterLevel);
}

float World::GetWaterLevel()
{
	return gFuncs->GetWaterLevel();
}

void World::SetMaximumFlightAltitude(float height)
{
	gFuncs->SetMaximumFlightAltitude(height);
}

float World::GetMaximumFlightAltitude()
{
	return GetMaximumFlightAltitude();
}

void World::SetKillCommandDelay(int32_t delay)
{
	gFuncs->SetKillCommandDelay(delay);
}

int32_t World::GetKillCommandDelay()
{
	return gFuncs->GetKillCommandDelay();
}

void World::SetVehiclesForcedRespawnHeight(float height)
{
	gFuncs->SetVehiclesForcedRespawnHeight(height);
}

float World::GetVehiclesForcedRespawnHeight()
{
	return gFuncs->GetVehiclesForcedRespawnHeight();
}

vcmpError World::CreateExplosion(int32_t worldId, int32_t type, Vector3 position, int32_t responsiblePlayerId, uint8_t atGroundLevel)
{
	vcmpError error = gFuncs->CreateExplosion(worldId, type, position.x, position.y, position.z, responsiblePlayerId, atGroundLevel);
	if (error)
		OutputError("Error in World::CreateExplosion. %s", GetErrorMessage(error).c_str());

	return error;
}

vcmpError World::PlaySound(int32_t worldId, int32_t soundId, Vector3 position)
{
	vcmpError error = gFuncs->PlaySound(worldId, soundId, position.x, position.y, position.z);
	if (error)
		OutputError("Error in World::PlaySound. %s", GetErrorMessage(error).c_str());

	return error;
}

void World::HideMapObject(int32_t modelId, int16_t tenthX, int16_t tenthY, int16_t tenthZ)
{
	gFuncs->HideMapObject(modelId, tenthX, tenthY, tenthZ);
}

void World::ShowMapObject(int32_t modelId, int16_t tenthX, int16_t tenthY, int16_t tenthZ)
{
	gFuncs->ShowMapObject(modelId, tenthX, tenthY, tenthZ);
}

void World::ShowAllMapObjects()
{
	gFuncs->ShowAllMapObjects();
}

vcmpError World::SetWeaponDataValue(int32_t weaponId, int32_t fieldId, double value)
{
	vcmpError error = gFuncs->SetWeaponDataValue(weaponId, fieldId, value);
	if (error)
		OutputError("Error in World::SetWeaponDataValue. %s", GetErrorMessage(error).c_str());

	return error;
}

double World::GetWeaponDataValue(int32_t weaponId, int32_t fieldId)
{
	return gFuncs->GetWeaponDataValue(weaponId, fieldId);
}

vcmpError World::ResetWeaponDataValue(int32_t weaponId, int32_t fieldId)
{
	vcmpError error = gFuncs->ResetWeaponDataValue(weaponId, fieldId);
	if (error)
		OutputError("Error in World::ResetWeaponDataValue. %s", GetErrorMessage(error).c_str());

	return error;
}

uint8_t World::IsWeaponDataValueModified(int32_t weaponId, int32_t fieldId)
{
	return gFuncs->IsWeaponDataValueModified(weaponId, fieldId);
}

vcmpError World::ResetWeaponData(int32_t weaponId)
{
	vcmpError error = gFuncs->ResetWeaponData(weaponId);
	if (error)
		OutputError("Error in World::ResetWeaponData. %s", GetErrorMessage(error).c_str());

	return error;
}

void World::ResetAllWeaponData()
{
	gFuncs->ResetAllWeaponData();
}
