#pragma once
#include "plugin.h"
#include "Vector3.h"

class World
{

private:

public:
	World() {};
	~World();

	void SetWorldBounds (float maxX, float minX, float maxY, float minY);
	void GetWorldBounds (float* maxXOut, float* minXOut, float* maxYOut, float* minYOut);
	void SetWastedSettings (uint32_t deathTimer, uint32_t fadeTimer, float fadeInSpeed, float fadeOutSpeed, uint32_t fadeColour, uint32_t corpseFadeStart, uint32_t corpseFadeTime);
	void GetWastedSettings (uint32_t* deathTimerOut, uint32_t* fadeTimerOut, float* fadeInSpeedOut, float* fadeOutSpeedOut, uint32_t* fadeColourOut, uint32_t* corpseFadeStartOut, uint32_t* corpseFadeTimeOut);
	void SetTimeRate (int32_t timeRate);
	int32_t GetTimeRate ();
	void SetHour (int32_t hour);
	int32_t GetHour ();
	void SetMinute (int32_t minute);
	int32_t GetMinute ();
	void SetWeather (int32_t weather);
	int32_t GetWeather ();
	void SetGravity (float gravity);
	float GetGravity ();
	void SetGameSpeed (float gameSpeed);
	float GetGameSpeed ();
	void SetWaterLevel (float waterLevel);
	float GetWaterLevel ();
	void SetMaximumFlightAltitude (float height);
	float GetMaximumFlightAltitude ();
	void SetKillCommandDelay (int32_t delay);
	int32_t GetKillCommandDelay ();
	void SetVehiclesForcedRespawnHeight (float height);
	float GetVehiclesForcedRespawnHeight ();

	vcmpError CreateExplosion (int32_t worldId, int32_t type, Vector3 position, int32_t responsiblePlayerId, uint8_t atGroundLevel);
	vcmpError PlaySound (int32_t worldId, int32_t soundId, Vector3 position);
	void HideMapObject (int32_t modelId, int16_t tenthX, int16_t tenthY, int16_t tenthZ);
	void ShowMapObject (int32_t modelId, int16_t tenthX, int16_t tenthY, int16_t tenthZ);
	void ShowAllMapObjects ();

	vcmpError SetWeaponDataValue (int32_t weaponId, int32_t fieldId, double value);
	double GetWeaponDataValue (int32_t weaponId, int32_t fieldId);
	vcmpError ResetWeaponDataValue (int32_t weaponId, int32_t fieldId);
	uint8_t IsWeaponDataValueModified (int32_t weaponId, int32_t fieldId);
	vcmpError ResetWeaponData (int32_t weaponId);
	void ResetAllWeaponData ();
};

