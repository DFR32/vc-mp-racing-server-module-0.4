#include "plugin.h"
#include "ConsoleMessages.h"
#include <cstdio>
#define PLUGIN_NAME "CPlusPlus Plugin"
#define PLUGIN_AUTHOR "Shadow"
#define PLUGIN_COPYRIGHT "Copyright (C) 2016 Shadow"
#define PLUGIN_HOSTNAME "CPPHost"
#define PLUGIN_VERSION 001
#define PLUGIN_STR_VERSION "0.0.1"

PluginFuncs * gFuncs;

extern uint8_t OnInitServer();
extern void OnShutdownServer();
extern void OnFrame(float fElapsedTime);
extern uint8_t OnInternalCommand(uint32_t commandIdentifier, const char* message);

extern void OnPlayerConnect(int32_t playerId);
extern uint8_t OnPlayerCommand(int32_t playerid, const char* params);
extern void OnPlayerUpdate(int32_t playerId, vcmpPlayerUpdate updateType);
extern void OnPlayerDisconnect(int32_t playerId, vcmpDisconnectReason reason);
extern void OnCheckpointEntered(int32_t checkpointID, int32_t playerID);
extern void OnPickupClaimPicked(int nPickupId, int nPlayerId);
extern void OnVehicleUpdate(int32_t nVehicleId, vcmpVehicleUpdate nUpdateType);
extern void OnPlayerExitVehicle(int32_t playerId, int32_t nVehicleId);
extern void OnPlayerSpawn(int32_t playerId);



extern "C" unsigned int VcmpPluginInit(PluginFuncs* functions, PluginCallbacks* callbacks, PluginInfo* info)
{
	OutputInfo("****************************************");
	OutputInfo("** Plug-in: %s Version: %s", PLUGIN_NAME, PLUGIN_STR_VERSION);
	OutputInfo("** Author: %s", PLUGIN_AUTHOR);
	OutputInfo("** Legal: %s", PLUGIN_COPYRIGHT);
	OutputInfo("****************************************");

	info->pluginVersion = PLUGIN_VERSION;
	info->apiMajorVersion = PLUGIN_API_MAJOR;
	info->apiMinorVersion = PLUGIN_API_MINOR;
	std::snprintf(info->name, sizeof(info->name), "%s", PLUGIN_HOSTNAME);

	gFuncs = functions;
	callbacks->OnPluginCommand = OnInternalCommand;

	callbacks->OnServerInitialise = OnInitServer;
	callbacks->OnServerShutdown = OnShutdownServer;
	callbacks->OnServerFrame = OnFrame;

	callbacks->OnPlayerConnect = OnPlayerConnect;
	callbacks->OnPlayerDisconnect = OnPlayerDisconnect;
	callbacks->OnPlayerUpdate = OnPlayerUpdate;
	callbacks->OnPlayerCommand = OnPlayerCommand;
	callbacks->OnCheckpointEntered = OnCheckpointEntered;
	//callbacks->OnPickupPicked = OnPickupClaimPicked;
	callbacks->OnVehicleUpdate = OnVehicleUpdate;
	callbacks->OnPlayerExitVehicle = OnPlayerExitVehicle;
	callbacks->OnPlayerSpawn = OnPlayerSpawn;
	return 1;
}
